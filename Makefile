# Copyright (C) 
# 2017 - Justin Freeston <s3547990@student.rmit.edu.au>
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

PROGRAM=MudGame
PROGRAM_DEBUG=$(PROGRAM)-debug
PROGRAM_DEBUG_VERBOSE=$(PROGRAM_DEBUG)-verbose

SOURCE_EXTENSION=cc
HEADER_EXTENSION=h
OBJECT_EXTENSION=o

CC=g++
LINK_FLAGS=-lboost_program_options -lboost_system -lboost_filesystem
FLAGS=-Wall -pedantic -std=c++14
FLAGS_DEBUG=-g
FLAGS_DEBUG_VERBOSE=-DDEBUG_FLAG

SOURCE_DIRECTORY=src/
OBJECT_DIRECTORY=obj/
SCRIPTS_DIRECTORY=scripts/

SOURCE_FILES=$(shell find $(SOURCE_DIRECTORY) -type f -name '*.$(SOURCE_EXTENSION)')
OBJECT_FILES=$(patsubst $(SOURCE_DIRECTORY)%.$(SOURCE_EXTENSION), $(OBJECT_DIRECTORY)%.$(OBJECT_EXTENSION), $(SOURCE_FILES))

.PHONY: all
all: clean_debug clean_debug_verbose compile 
	$(CC) $(LINK_FLAGS) -o $(PROGRAM) $(OBJECT_FILES)

.PHONY: debug
debug: clean_all clean_debug_verbose compile_debug
	$(CC) $(LINK_FLAGS) -o $(PROGRAM_DEBUG) $(OBJECT_FILES)

.PHONY: debug_verbose
debug_verbose: clean_all clean_debug compile_debug_verbose
	$(CC) $(LINK_FLAGS) -o $(PROGRAM_DEBUG_VERBOSE) $(OBJECT_FILES)

compile_debug: FLAGS += $(FLAGS_DEBUG)
compile_debug: compile

compile_debug_verbose: FLAGS += $(FLAGS_DEBUG) $(FLAGS_DEBUG_VERBOSE)
compile_debug_verbose: compile

clean_all:
ifneq ("$(wildcard $(PROGRAM))", "")
clean_all: clean
endif	

clean_debug:
ifneq ("$(wildcard $(PROGRAM_DEBUG))", "")
clean_debug: clean
endif

clean_debug_verbose:
ifneq ("$(wildcard $(PROGRAM_DEBUG_VERBOSE))", "")
clean_debug_verbose: clean
endif

compile: $(OBJECT_FILES)

.PHONY: clean
clean:
	rm -f $(PROGRAM) $(PROGRAM_DEBUG) $(PROGRAM_DEBUG_VERBOSE)
	rm -r $(OBJECT_DIRECTORY)

.PHONY: archive
archive:
	zip $(USER)-A2-$(PROGRAM) -r $(SOURCE_DIRECTORY) Makefile

vpath %.$(SOURCE_EXTENSION) $(SOURCE_DIRECTORY)
vpath %.$(HEADER_EXTENSION) $(SOURCE_DIRECTORY)

$(OBJECT_DIRECTORY)mud_game.o: mud_game.cc mud_game.h command_line_args.h game/game.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)command_line_args.o: command_line_args.cc command_line_args.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)utility.o: utility.cc utility.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)game/game.o: game/game.cc game/game.h game/game_paths.h command_line_args.h controller/controller.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)controller/controller.o: controller/controller.cc controller/controller.h controller/ascii_loader.h controller/ascii_saver.h controller/command.h game/game_paths.h model/model.h view/view.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)controller/ascii_loader.o: controller/ascii_loader.cc controller/ascii_loader.h controller/exception.h controller/enum_information.h utility.h model/model.h model/item/item.h model/location/room.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)controller/ascii_saver.o: controller/ascii_saver.cc controller/ascii_saver.h controller/exception.h model/model.h model/character/player.h model/item/item.h model/location/area.h model/location/room.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)controller/enum_information.o: controller/enum_information.cc controller/enum_information.h utility.h model/exception.h model/item/item.h model/location/room.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)controller/command.o: controller/command.cc controller/command.h controller/enum_information.h utility.h model/location/room.h model/model.h model/character/character_exception.h model/character/player.h model/character/shopkeeper.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)view/view.o: view/view.cc view/view.h controller/controller.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)model/model.o: model/model.cc model/model.h model/exception.h model/character/character.h model/character/player.h model/character/shopkeeper.h model/item/item.h model/location/area.h model/location/room.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)model/character/character.o: model/character/character.cc model/character/character.h model/character/character_exception.h model/item/item.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)model/character/player.o: model/character/player.cc model/character/player.h model/character/character.h model/character/character_exception.h model/item/item.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@
	
$(OBJECT_DIRECTORY)model/character/shopkeeper.o: model/character/shopkeeper.cc model/character/shopkeeper.h model/character/character.h model/character/character_exception.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)model/item/item.o: model/item/item.cc model/item/item.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)model/location/area.o: model/location/area.cc model/location/area.h
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

$(OBJECT_DIRECTORY)model/location/room.o: model/location/room.cc model/location/room.h model/exception.h 
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -c $< -o $@

