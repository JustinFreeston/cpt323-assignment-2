```
Student Name:  Justin Freeston
Student ID:    s3547990
Student Email: s3547990@student.rmit.edu.au

Course:   CPT323 Opject-Oriented Programming in C++ - SP1 2017
Lecturer: Paul Miller (paul.miller@rmit.edu.au)
Material: Assignment 2 - MUD Game
```
Git repo available at https://bitbucket.org/JustinFreeston/cpt323-assignment-2/


MUD Game
======
This program is a game in which a player explores rooms, opening and closing
doors along the way, buys and wears armor and weapons and manages an account
and inventory.

Compiling
------
Simply use the command `make` when in the project directory to compile the
program. This will compile an executable file called `MudGame`.

Note: when compiling on the RMIT syste,s the g++ command must be sourced by
using `source /opt/rh/devtools-6/enable`.

Program usage
------
When calling the program from the command line, the location of the data files
must be specified. These data files can either be in ascii or binary format,
and should be called using the appropriate command line argument. Additionally
the state of the game can be saved upon exit in ascii and/or binary format.

* **--ascii_load**:
Load data from the ascii files in the directory. These files are items.txt,
areas.txt, rooms.txt, exits.txt and players.txt. This must point to a valid
directory. Must not be used along with --binary_load.
* **--binary_load**:
Load data from a binary file. This must point to a valid binary file. Must not
be used along with --ascii_load.
* **--ascii_save**:
Save data as ascii to the directory. All ascii files will be created if they
don't exist, otherwise exits.txt and players.txt will be overwritten with new
data. This must point to a valid directory.
* **--binary_load**:
Save data to a binary file. This file will be created if it does not exist,
otherwise it will overwrite an existing file with new data.

License
------
Licensed under the MIT License.

Catch header file available at https://github.com/philsquared/Catch and licensed under the Boost Software License.