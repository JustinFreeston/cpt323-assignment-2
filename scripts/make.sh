#!/bin/bash

MAKEFILE_RELATIVE_DIRECTORY="../"
SCRIPT_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/"
ABSOLUTE_DIRECTORY="$( cd "$SCRIPT_DIRECTORY""$MAKEFILE_RELATIVE_DIRECTORY" && pwd )/"
FLAGS="-C "$ABSOLUTE_DIRECTORY""

clear
source /opt/rh/devtoolset-6/enable

if [ -z "$1" ]
then
    make $FLAGS
else
    make $FLAGS "$1"
fi
