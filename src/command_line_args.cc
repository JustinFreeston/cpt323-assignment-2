/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "command_line_args.h"

namespace po = boost::program_options;

CommandLineArgs::CommandLineArgs(int argc, char** argv) {
  LoadDescription();

  try {
    po::store(po::parse_command_line(argc, argv, description_), var_map_);
  }
  catch (const po::invalid_command_line_syntax& e) {
    throw e;
  }
  catch (const po::unknown_option& e) {
    throw e;
  }

  LoadVariables();
  LoadHelp();
}

void CommandLineArgs::LoadDescription(void) {
  description_.add_options()
    ("help,h", "Display help")
    ("ascii_load", po::value<std::string>(),
      "Specify ascii load directory (must not be used with binary_load)")
    ("binary_load", po::value<std::string>(),
      "Specify binary load file (must not be used with ascii_load)")
    ("ascii_save", po::value<std::string>(), "Specify ascii save directory")
    ("binary_save", po::value<std::string>(), "Specify binary save file");
}

void CommandLineArgs::LoadVariables(void) {
  try {
    ascii_load_ = var_map_["ascii_load"].as<std::string>();
  }
  catch (const std::exception& ignored) {}

  try {
    binary_load_ = var_map_["binary_load"].as<std::string>();
  }
  catch (const std::exception& ignored) {}

  try {
    ascii_save_ = var_map_["ascii_save"].as<std::string>();
  }
  catch (const std::exception& ignored) {}

  try {
    binary_save_ = var_map_["binary_save"].as<std::string>();
  }
  catch (const std::exception& ignored) {}
}

