/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_COMMAND_LINE_ARGS_H_
#define MUDGAME_SRC_COMMAND_LINE_ARGS_H_

#include <string>

#include <boost/program_options.hpp>

class CommandLineArgs {
 public:
  CommandLineArgs(int argc, char** argv);
  inline bool IsHelp(void) const {
    if (var_map_.count("help") || var_map_.count("h")) {
      return true;
    }
    return false;
  }
  inline const std::string& Help(void) const {
    return help_;
  }
  inline const std::string& AsciiLoad(void) const {
    return ascii_load_;
  }
  inline const std::string& BinaryLoad(void) const {
    return binary_load_;
  }
  inline const std::string& AsciiSave(void) const {
    return ascii_save_;
  }
  inline const std::string& BinarySave(void) const {
    return binary_save_;
  }

 private:
  boost::program_options::options_description description_;
  boost::program_options::variables_map var_map_;
  std::string help_;
  std::string ascii_load_;
  std::string binary_load_;
  std::string ascii_save_;
  std::string binary_save_;
  void LoadDescription(void);
  void LoadVariables(void);
  inline void LoadHelp(void) {
    help_ = "MudGame help:\n" \
      "  ascii_load <dir> - Load data as ascii from dir. " \
      "Must not be used with binary_load.\n" \
      "  binary_load <file> - Load data as binary from file. " \
      "Must not be used with ascii_load.\n" \
      "  ascii_save <dir> - Save data as ascii to dir.\n" \
      "  binary_save <file> - Save data as binary to file.";
  }
};

#endif  // MUDGAME_SRC_COMMAND_LINE_ARGS_H_
