/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "ascii_loader.h"

namespace controller {

void AsciiLoader::LoadItems(const std::string& read_path) {
  std::ifstream items_file(read_path);
  // Path doesn't exist or is not accessible
  if (!items_file.good()) {
    throw IOError("Could not open items file");
  }

  // Process each line
  int line_number = 0;
  std::string line;
  while (std::getline(items_file, line)) {
    ++line_number;
    try {
      CreateItem(line);
    }
    catch (...) {
      items_file.close();
      throw;
    }
  }
  items_file.close();
}

void AsciiLoader::CreateItem(const std::string& line) {
  std::vector<std::string> split = utility::Utility::Split(line, '\t');
  if (split.size() != kItemArguments) {
    throw InvalidFormat("Not enough arguments");
  }

  int item_id;
  model::item::WearLocation wear_location;
  std::string* name;
  std::string* description;
  int market_price;

  bool is_weapon;

  try {
    item_id = std::stoi(split[0]);
    if (item_id < 1) {
      throw InvalidFormat("item_id cannot be less than 1");
    }
    if (split[1] == "WEAPON") {
      is_weapon = true;
    } else {
      wear_location = EnumInformation::GetWearLocationInformation(split[1], true, false).GetWearLocation();
    }
    name = &split[2];
    description = &split[3];
    market_price = std::stoi(split[4]);
  }
  catch (...) {
    throw InvalidFormat("Could not parse items line");
  }

  try {
    std::unique_ptr<model::item::Item> item;
    if (is_weapon) {
      item = std::make_unique<model::item::Weapon>(item_id, *name, *description, market_price);
    } else {
      item = std::make_unique<model::item::Armor>(item_id, *name, *description, market_price, wear_location);
    }
    model_.AddItem(std::move(item));
  }
  catch (...) {
    throw;
  }
}

void AsciiLoader::LoadAreas(const std::string& read_path) {
  std::ifstream areas_file(read_path);
  // Path doesn't exist or is not accessible
  if (!areas_file.good()) {
    throw IOError("Could not open areas file");
  }

  // Process each line
  int line_number = 0;
  std::string line;
  while (std::getline(areas_file, line)) {
    ++line_number;
    try {
      CreateArea(line);
    }
    catch (...) {
      areas_file.close();
      throw;
    }
  }
  areas_file.close();
}

void AsciiLoader::CreateArea(const std::string& line) {
  std::vector<std::string> split = utility::Utility::Split(line, '\t');
  if (split.size() != kAreaArguments) {
    throw InvalidFormat("Not enough arguments");
  }
  
  int area_id;
  std::string* name;
  std::string* description;

  try {
    area_id = std::stoi(split[0]);
    if (area_id < 1) {
      throw InvalidFormat("area_id cannot be less than 1");
    }
    name = &split[1];
    description = &split[2];
  }
  catch (...) {
    throw InvalidFormat("Could not parse areas line");
  }

  try {
    std::unique_ptr<model::location::Area> area = std::make_unique<
          model::location::Area>(area_id, *name, *description);
    model_.AddArea(std::move(area));
  }
  catch (...) {
    throw;
  }
}

void AsciiLoader::LoadRooms(const std::string& read_path) {
  std::ifstream rooms_file(read_path);
  // Path doesn't exist or is not accessible
  if (!rooms_file.good()) {
    throw IOError("Could not open rooms file");
  }

  // Process each line
  int line_number = 0;
  std::string line;
  while (std::getline(rooms_file, line)) {
    ++line_number;
    try {
      CreateRoom(line);
    }
    catch (...) {
      rooms_file.close();
      throw;
    }
  }
  rooms_file.close();
}

void AsciiLoader::CreateRoom(const std::string& line) {
  std::vector<std::string> split = utility::Utility::Split(line, '\t');
  if (split.size() != kRoomArguments) {
    throw InvalidFormat("Not enough arguments");
  }

  int global_id;
  int local_id;
  int area_id;
  std::string* name;
  std::string* description;
  
  try {
    global_id = std::stoi(split[0]);
    local_id = std::stoi(split[1]);
    area_id = std::stoi(split[2]);
    if (global_id < 1 || local_id < 1 || area_id < 1) {
      throw InvalidFormat("global, local, and area ids cannot be less than 1");
    }
    name = &split[3];
    description = &split[4];
  }
  catch (...) {
    throw InvalidFormat("Could not parse rooms line");
  }

  try {
    std::unique_ptr<model::location::Room> room = std::make_unique<
          model::location::Room>(global_id, local_id, area_id, *name,
          *description);

    model_.AddRoom(std::move(room));
  }
  catch (...) {
    throw;
  }
}

void AsciiLoader::LoadConnections(const std::string& read_path) {
  std::ifstream connections_file(read_path);
  // Path doesn't exist or is not accessible
  if (!connections_file.good()) {
    throw IOError("Could not open connections file");
  }

  // Process each line
  int line_number = 0;
  std::string line;
  while (std::getline(connections_file, line)) {
    ++line_number;
    try {
      CreateConnection(line);
    }
    catch (...) {
      connections_file.close();
      throw;
    }
  }
  connections_file.close();
}

void AsciiLoader::CreateConnection(const std::string& line) {
  std::vector<std::string> split = utility::Utility::Split(line, '\t');
  if (split.size() != kConnectionArguments) {
    throw InvalidFormat("Not enough arguments");
  }
  
  int origin_id;
  int destination_id;
  model::location::Direction direction;
  bool open;

  try {
    origin_id = std::stoi(split[0]);
    destination_id = std::stoi(split[1]);
    if (origin_id < 1 || destination_id < 1) {
      throw InvalidFormat("origin, and destination ids cannot be less than 1");
    }
    direction = EnumInformation::GetDirectionInformation(split[2], true, false).GetDirection();
    if (split[3] == "true") {
      open = true;
    } else if (split[3] == "false") {
      open = false;
    } else {
      throw InvalidFormat("Door open must be boolean true or false");
    }
  }
  catch (...) {
    throw InvalidFormat("Could not parse connections line");
  }

  try {
    std::unique_ptr<model::location::Connection> connection = std::make_unique<
          model::location::Connection>(origin_id, destination_id, direction,
          open);
    model_.AddConnection(std::move(connection));
  }
  catch (...) {
    throw;
  }
}

void AsciiLoader::LoadPlayers(const std::string& read_path) {
  std::ifstream players_file(read_path);
  // Path doesn't exist or is not accessible
  if (!players_file.good()) {
    throw IOError("Could not open players file");
  }

  // Process each line
  int line_number = 0;
  std::string line;
  while (std::getline(players_file, line)) {
    ++line_number;
    try {
      CreatePlayer(line);
    }
    catch (...) {
      players_file.close();
      throw;
    }
  }
  players_file.close();
}

void AsciiLoader::CreatePlayer(const std::string& line) {
  std::vector<std::string> split = utility::Utility::Split(line, '\t');
  // Could have more depending on inventory_count
  if (split.size() < kPlayerArguments) {
    throw InvalidFormat("Not enough arguments");
  }

  int id;
  std::string* username;
  unsigned long long password_hash;
  int location_id;
  int wrists_id = 0;
  int arms_id = 0;
  int hands_id = 0;
  int legs_id = 0;
  int feet_id = 0;
  int head_id = 0;
  int torso_id = 0;
  int shield_id = 0;
  int weapon_id = 0;
  int inventory_count;
  std::vector<int> inventory_ids;
  int gold;
  int intelligence;
  int wisdom;
  int strength;
  int dexterity;
  int constitution;
  int hitpoints;
  int mana;
  int moves;

  try {
    id = std::stoi(split[0]);
    if (id < 1) {
      throw InvalidFormat("id cannot be less than 1");
    }
    username = &split[1];
    password_hash = std::stoull(split[2]);
    location_id = std::stoi(split[3]);
    if (split[4] != kNull) {
      wrists_id = std::stoi(split[4]);
    }
    if (split[5] != kNull) {
      arms_id = std::stoi(split[5]);
    }
    if (split[6] != kNull) {
      hands_id = std::stoi(split[6]);
    }
    if (split[7] != kNull) {
      legs_id = std::stoi(split[7]);
    }
    if (split[8] != kNull) {
      feet_id = std::stoi(split[8]);
    }
    if (split[9] != kNull) {
      head_id = std::stoi(split[9]);
    }
    if (split[10] != kNull) {
      torso_id = std::stoi(split[10]);
    }
    if (split[11] != kNull) {
      shield_id = std::stoi(split[11]);
    }
    if (split[12] != kNull) {
      weapon_id = std::stoi(split[12]);
    }
    inventory_count = std::stoi(split[13]);
    for (int i = 14; i < 14 + inventory_count; i++) {
      inventory_ids.push_back(std::stoi(split[i]));
    }
    gold = std::stoi(split[14 + inventory_count]);
    intelligence = std::stoi(split[15 + inventory_count]);
    wisdom = std::stoi(split[16 + inventory_count]);
    strength = std::stoi(split[17 + inventory_count]);
    dexterity = std::stoi(split[18 + inventory_count]);
    constitution = std::stoi(split[19 + inventory_count]);
    hitpoints = std::stoi(split[20 + inventory_count]);
    mana = std::stoi(split[21 + inventory_count]);
    moves = std::stoi(split[22 + inventory_count]);
  }
  catch (...) {
    throw InvalidFormat("Could not parse players line");
  }

  try {
    std::unique_ptr<model::character::Player> player =
          std::make_unique<model::character::Player>(id, *username, password_hash,
          intelligence, wisdom, dexterity, strength, constitution, hitpoints,
          mana, location_id, moves, gold);
    model_.AddPlayer(std::move(player));

    // These ids should probably be checked to be the correct wear_location
    if (wrists_id) {
      model_.AddToInventory(id, model_.Item(wrists_id));
      model_.EquipItem(id, model_.Item(wrists_id));
    }
    if (arms_id) {
      model_.AddToInventory(id, model_.Item(arms_id));
      model_.EquipItem(id, model_.Item(arms_id));
    }
    if (hands_id) {
      model_.AddToInventory(id, model_.Item(hands_id));
      model_.EquipItem(id, model_.Item(hands_id));
    }
    if (legs_id) {
      model_.AddToInventory(id, model_.Item(legs_id));
      model_.EquipItem(id, model_.Item(legs_id));
    }
    if (feet_id) {
      model_.AddToInventory(id, model_.Item(feet_id));
      model_.EquipItem(id, model_.Item(feet_id));
    }
    if (head_id) {
      model_.AddToInventory(id, model_.Item(head_id));
      model_.EquipItem(id, model_.Item(head_id));
    }
    if (torso_id) {
      model_.AddToInventory(id, model_.Item(torso_id));
      model_.EquipItem(id, model_.Item(torso_id));
    }
    if (shield_id) {
      model_.AddToInventory(id, model_.Item(shield_id));
      model_.EquipItem(id, model_.Item(shield_id));
    }
    if (weapon_id) {
      model_.AddToInventory(id, model_.Item(weapon_id));
      model_.EquipItem(id, model_.Item(weapon_id));
    }

    for (const auto& id : inventory_ids) {
      model_.AddToInventory(id, model_.Item(id));
    }
  }
  catch (...) {
    throw;
  }
}

}  // namespace controller
