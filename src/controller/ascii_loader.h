/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_CONTROLLER_ASCII_LOADER_H_
#define MUDGAME_SRC_CONTROLLER_ASCII_LOADER_H_

#include <fstream>
#include <memory>
#include <string>
#include <vector>

#include "exception.h"
#include "enum_information.h"
#include "../utility.h"
#include "../model/model.h"
#include "../model/item/item.h"
#include "../model/location/room.h"

namespace controller {

class AsciiLoader {
 public:
  // Constructors
  explicit AsciiLoader(model::Model& model) : model_(model) {}

  // Operations
  void LoadItems(const std::string& read_path);
  void LoadAreas(const std::string& read_path);
  void LoadRooms(const std::string& read_path);
  void LoadConnections(const std::string& read_path);
  void LoadPlayers(const std::string& read_path);

 private:
  // Attributes
  const std::string kNull = "NULL";
  const unsigned kItemArguments = 5;
  const unsigned kAreaArguments = 3;
  const unsigned kRoomArguments = 5;
  const unsigned kConnectionArguments = 4;
  const unsigned kPlayerArguments = 23;
  model::Model& model_;

  // Operations
  void CreateItem(const std::string& line);
  void CreateArea(const std::string& line);
  void CreateRoom(const std::string& line);
  void CreateConnection(const std::string& line);
  void CreatePlayer(const std::string& line);
};

}  // namespace controller

#endif  // MUDGAME_SRC_CONTROLLER_ASCII_LOADER_H_
