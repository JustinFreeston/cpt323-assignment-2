/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "ascii_saver.h"

namespace controller {

void AsciiSaver::SaveItems(const std::string& save_path) {
  // Check if the file exists, don't need to change it
  std::ifstream existing_file(save_path);
  if (existing_file.good()) {
    return;
  }

  std::ofstream items_file(save_path);
  // Check the file can be written to 
  if (!items_file.is_open()) {
    throw IOError("Could not write items file");
  }

  // Write data
  for (const auto& pair : kModel.Items()) {
    const auto& item = *pair.second;
//    items_file << item << '\n';
    items_file << item.ToString() << '\n';
  }

  items_file.close();
}

void AsciiSaver::SaveAreas(const std::string& save_path) {
  // Check if the file exists, don't need to change it
  std::ifstream existing_file(save_path);
  if (existing_file.good()) {
    return;
  }

  std::ofstream areas_file(save_path);
  // Check the file can be written to 
  if (!areas_file.is_open()) {
    throw IOError("Could not write areas file");
  }

  // Write data
  for (const auto& pair : kModel.Areas()) {
    const auto& area = *pair.second;
//    areas_file << area << '\n';
    areas_file << area.ToString() << '\n';
  }

  areas_file.close();
}

void AsciiSaver::SaveRooms(const std::string& save_path) {
  // Check if the file exists, don't need to change it
  std::ifstream existing_file(save_path);
  if (existing_file.good()) {
    return;
  }

  std::ofstream rooms_file(save_path);
  // Check the file can be written to 
  if (!rooms_file.is_open()) {
    throw IOError("Could not write rooms file");
  }

  // Write data
  for (const auto& pair : kModel.Rooms()) {
    const auto& room = *pair.second;
//    rooms_file << room << '\n';
    rooms_file << room.ToString() << '\n';
  }

  rooms_file.close();
}

void AsciiSaver::SaveConnections(const std::string& save_path) {
  std::ofstream connections_file(save_path);
  // Check the file can be written to 
  if (!connections_file.is_open()) {
    throw IOError("Could not write connections file");
  }

  // Write data
  for (const auto& pair : kModel.Connections()) {
    const auto& connection = *pair.second;
//    connections_file << connection << '\n';
    connections_file << connection.ToString() << '\n';
  }

  connections_file.close();
}

void AsciiSaver::SavePlayers(const std::string& save_path) {
  std::ofstream players_file(save_path);
  // Check the file can be written to 
  if (!players_file.is_open()) {
    throw IOError("Could not write players file");
  }

  // Write data
  for (const auto& pair : kModel.Players()) {
    const auto& player = *pair.second;
//    players_file << player << '\n';
    players_file << player.ToString() << '\n';
  }

  players_file.close();
}

}  // namespace controller
