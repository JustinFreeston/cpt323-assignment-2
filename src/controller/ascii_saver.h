/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_CONTROLLER_ASCII_SAVER_H_
#define MUDGAME_SRC_CONTROLLER_ASCII_SAVER_H_

#include <fstream>
#include <string>

#include "exception.h"
#include "../model/model.h"
#include "../model/character/player.h"
#include "../model/item/item.h"
#include "../model/location/area.h"
#include "../model/location/room.h"

namespace controller {

class AsciiSaver {
 public:
  // Constructors;
  explicit AsciiSaver(const model::Model& model) : kModel(model) {}

  // Operations
  void SaveItems(const std::string& save_path);
  void SaveAreas(const std::string& save_path);
  void SaveRooms(const std::string& save_path);
  void SaveConnections(const std::string& save_path);
  void SavePlayers(const std::string& save_path);

 private:
  // Attributes
  const model::Model& kModel;
};

}  // namespace controller

#endif  // MUDGAME_SRC_CONTROLLER_ASCII_SAVER_H_
