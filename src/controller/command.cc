/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "command.h"

namespace controller {

std::string LookCommand::Perform(int player_id, const std::vector<std::string>& args) {
  if (args.size() == 1) {
    return LookRoom(player_id);
  }
  if (args.size() == 2) {
    try {
      const DirectionInformation& direction_information = EnumInformation::GetDirectionInformation(args[1], false, true);
      return LookRoom(player_id, direction_information);
    }
    // Ignored, simply means second argument isn't a direction
    catch (...) {}
  }
  std::string joined = utility::Utility::Join(args, " ", 1, args.size());
  const auto& items = model_.FindItemsByName(joined);
  if (items.size() == 0) {
    return (boost::format("Error: No items found with name starting with %s.") % joined).str();
  }
  else if (items.size() == 1) {
    return LookItem(player_id, *(items.front()));
  } else {
    std::string error_string = "Error: Multiple items found starting with \"";
    error_string += joined;
    error_string += "\"";
    for (const auto& item : items) {
      error_string += "\n    - ";
      error_string += item->Name();
    }
    return error_string;
  }
  return "Error: Unknown arguments for look command.";
}

std::string LookCommand::LookRoom(int player_id) const {
  auto& player = model_.Player(player_id);
  auto& room = model_.Room(player.LocationId());
  auto& connection_ids = room.ConnectionIds();

  std::string output = room.Name();
  output += '\n';
  output += room.Description(); 

  // Add open exits
  for (int i = 0; i < model::location::kTotalDirections; ++i) {
    int connection_id = connection_ids[i];
    if (connection_id != model::location::kNullConnection) {
      auto& connection = model_.Connection(connection_id);
      if (connection.Open()) {
        output += (boost::format("\n  Exit available %s.") % EnumInformation::GetDirectionInformation(i).GetString()).str();
      }
    }
  }

  return output;
}

std::string LookCommand::LookRoom(int player_id, const DirectionInformation& direction_information) const {
  auto& player = model_.Player(player_id);
  int player_location = player.LocationId();
  auto& room = model_.Room(player_location);
  int connection_id = room.ConnectionIds()[direction_information.GetValue()];

  const std::string& direction_string = direction_information.GetString();
  if (connection_id == model::location::kNullConnection) {
    return (boost::format("There isn't a room to the %s.") % direction_string).str();
  }

  auto& connection = model_.Connection(connection_id);
  if (connection.Open()) {
    std::string next_room_name;
    if (connection.OriginId() == player_location) {
      next_room_name = model_.Room(connection.DestinationId()).Name();
    } else {
      next_room_name = model_.Room(connection.OriginId()).Name();
    }
    return (boost::format("You see %s to the %s.") % next_room_name % direction_string).str();
  } else {
    return (boost::format("The door to the %s is closed.") % direction_string).str();
  }
}

std::string LookCommand::LookItem(int player_id, const model::item::Item& item) const {
  std::string output = "Name: ";
  output += item.Name();

  const model::item::Armor* armor = dynamic_cast<const model::item::Armor*>(&item);
  if (armor != nullptr) {
    output += "\nType: ";
    output += EnumInformation::GetWearLocationInformation(armor->GetWearLocation()).GetString();
  } else {
    const model::item::Weapon* weapon = dynamic_cast<const model::item::Weapon*>(&item);
    if (weapon != nullptr) {
      output += "\nType: WEAPON";
    }
  }

  output += "\nDescription: ";
  output += item.Description();
  return output;
}

std::string OpenCommand::Perform(int player_id, const std::vector<std::string>& args) {
  if (args.size() == 1) {
    return "Error: Missing direction argument.";
  }
  else if (args.size() > 2) {
    return "Error: Too many arguments, only expect direction.";
  }

  try {
    const DirectionInformation& direction_information = EnumInformation::GetDirectionInformation(args[1], false, true);
    return Open(player_id, direction_information);
  }
  catch (...) {
    return "Error: Could not parse direction.";
  }
}

std::string OpenCommand::Open(int player_id, const DirectionInformation& direction_information) {
  auto& player = model_.Player(player_id);
  auto& room = model_.Room(player.LocationId());

  int connection_id = room.ConnectionIds()[direction_information.GetValue()];

  if (connection_id == model::location::kNullConnection) {
    return (boost::format("There isn't a door to open to the %s.") % direction_information.GetString()).str();
  }

  if (model_.SetDoorStatus(connection_id, true)) {
    return (boost::format("Opened the door to the %s.") % direction_information.GetString()).str();
  } else {
    return (boost::format("The door to the %s is already open.") % direction_information.GetString()).str();
  }
}

std::string CloseCommand::Perform(int player_id, const std::vector<std::string>& args) {
  if (args.size() == 1) {
    return "Error: Missing direction argument.";
  }
  else if (args.size() > 2) {
    return "Error: Too many arguments, only expect direction.";
  }

  try {
    const DirectionInformation& direction_information = EnumInformation::GetDirectionInformation(args[1], false, true);
    return Close(player_id, direction_information);
  }
  catch (...) {
    return "Error: Could not parse direction.";
  }
}

std::string CloseCommand::Close(int player_id, const DirectionInformation& direction_information) {
  auto& player = model_.Player(player_id);
  auto& room = model_.Room(player.LocationId());

  int connection_id = room.ConnectionIds()[direction_information.GetValue()];

  if (connection_id == model::location::kNullConnection) {
    return (boost::format("There isn't a door to close to the %s.") % direction_information.GetString()).str();
  }

  if (model_.SetDoorStatus(connection_id, false)) {
    return (boost::format("Closed the door to the %s.") % direction_information.GetString()).str();
  } else {
    return (boost::format("The door to the %s is already closed.") % direction_information.GetString()).str();
  }
}

std::string WearCommand::Perform(int player_id, const std::vector<std::string>& args) {
  if (args.size() == 1) {
    return "Error: Missing item name arguments.";
  }

  std::string joined = utility::Utility::Join(args, " ", 1, args.size());
  return Wear(player_id, joined);
}

std::string WearCommand::Wear(int player_id, const std::string& item_name) {
  auto items = model_.FindItemsByName(item_name);

  if (items.size() == 0) {
    return "Could not find any matching item names.";
  } else if (items.size() > 1) {
    return (boost::format("Item name too vague. Found %d item names starting with %s.") % items.size() % item_name).str();
  }

  const model::item::Item& item = *items.front();

  try {
    model_.EquipItem(player_id, item);
    return (boost::format("Equipped %s.") % item.Name()).str();
  }
  catch (const model::character::NotInInventory& e) {
    return (boost::format("The item %s is not in your inventory!") % item.Name()).str();
  }
  catch (const model::character::WearLocationOccupied& e) {
    return "An item is already equipped in that slot!";
  }
  catch (const model::UnequippableItem& e) {
    return (boost::format("The item %s is unequippable!") % item.Name()).str();
  }
}

std::string RemoveCommand::Perform(int player_id, const std::vector<std::string>& args) {
  if (args.size() == 1) {
    return "Error: Missing item name arguments.";
  }

  std::string joined = utility::Utility::Join(args, " ", 1, args.size());
  return Remove(player_id, joined);
}

std::string RemoveCommand::Remove(int player_id, const std::string& item_name) {
  auto items = model_.FindItemsByName(item_name);

  if (items.size() == 0) {
    return "Could not find any matching item names.";
  } else if (items.size() > 1) {
    return "Item name too vague.";
  }

  auto& player = model_.Player(player_id);
  const model::item::Item& item = *items.front();
  if (player.WeaponId() == item.Id()) {
    model_.UnequipWeapon(player_id);
    return (boost::format("Unequipped weapon %s and placed in inventory.") % item.Name()).str();
  }
  
  auto& equipped_armor = player.ArmorIds();
  auto position = std::find(equipped_armor.begin(), equipped_armor.end(), item.Id());
  if (position != equipped_armor.end()) {
    const model::item::Armor* armor = dynamic_cast<const model::item::Armor*>(&item);
    model_.UnequipArmor(player_id, armor->GetWearLocation());
    return (boost::format("Unequipped armor %s and placed in inventory.") % item.Name()).str();
  } else {
    return (boost::format("The item %s is not equipped.") % item.Name()).str();
  }
}

std::string MoveCommand::Perform(int player_id, const std::vector<std::string>& args) {
  if (args.size() > 1) {
    return "Error: Too many arguments.";
  }

  const DirectionInformation& direction_information = EnumInformation::GetDirectionInformation(args[0], false, true);
  return Move(player_id, direction_information);
}

std::string MoveCommand::Move(int player_id, const DirectionInformation& direction_information) {
  auto& player = model_.Player(player_id);
  auto& room = model_.Room(player.LocationId());
  
  int connection_id = room.ConnectionIds()[direction_information.GetValue()];
  if (connection_id == model::location::kNullConnection) {
    return (boost::format("There isn't a room to the %s.") % direction_information.GetString()).str();
  }

  auto& connection = model_.Connection(connection_id);
  if (!connection.Open()) {
    return (boost::format("The door to the %s is closed.") % direction_information.GetString()).str();
  }

  int room_destination_id = connection.OriginId();
  if (room_destination_id == room.GlobalId()) {
    room_destination_id = connection.DestinationId();
  }

  try {
    model_.Move(player_id, model_.Room(room_destination_id).GlobalId());
  }
  catch (const model::PlayerExhausted& e) {
    return "You are too exhausted to move!";
  }

  return (boost::format("You moved %s.") % direction_information.GetString()).str();
}

std::string ListCommand::Perform(int player_id, const std::vector<std::string>& args) {
  if (args.size() > 1) {
    return "Error: Too many arguments.";
  }

  return List(player_id);
}

std::string ListCommand::List(int player_id) const {
  auto& player = model_.Player(player_id);
  auto& room = model_.Room(player.LocationId());
  
  const auto shopkeepers = model_.ShopkeepersInRoom(room);
  if (shopkeepers.size() == 0) {
    return "There isn't a shopkeeper here.";
  }

  std::string output = "Shopkeepers and their wares:";
  for (const auto& shopkeeper : shopkeepers) {
    const auto& ware_ids = shopkeeper->WareIds();
    if (ware_ids.size() != 0) {
      output += "\n  Wares from ";
      output += shopkeeper->Name();
      output += (boost::format("\n    %-3s%-20s%-3s") % "ID" % "Name" % "Price").str();
      for (const auto& item_id : ware_ids) {
        const auto& item = model_.Item(item_id);
        output += (boost::format("\n    %-3d%-20s%-3d") % item.Id() % item.Name() % item.MarketPrice()).str();
      }
    }
  }
  return output;
}

std::string BuyCommand::Perform(int player_id, const std::vector<std::string>& args) {
  if (args.size() == 1) {
    return "Error: Missing item name/id arguments.";
  }

  std::string joined = utility::Utility::Join(args, " ", 1, args.size());
  return Buy(player_id, joined);
}

std::string BuyCommand::Buy(int player_id, const std::string& item_name) {
  const auto& room = model_.Room(model_.Player(player_id).LocationId());
  const auto& items = model_.FindItemsByName(item_name);

  if (items.size() > 1) {
    return "Item name too vague.";
  }

  int item_id;
  if (items.size() == 1) {
    item_id = items.front()->Id();
  } else {
    try {
      int temp_item_id = std::stoi(item_name);
      // Make sure temp_item_id is valid before assigning
      item_id = model_.Item(temp_item_id).Id();
    }
    catch (...) {
      return "Could not find any matching items.";
    }
  }

  bool selling;
  for (const auto& shopkeeper : model_.ShopkeepersInRoom(room)) {
    const auto& ware_ids = shopkeeper->WareIds();
    auto position = std::find(ware_ids.begin(), ware_ids.end(), item_id);
    if (position != ware_ids.end()) {
      selling = true;
      break;
    }
  }

  const auto& item = model_.Item(item_id);

  if (!selling) {
    return (boost::format("There isn't a shopkeeper selling %s in this room.") % item.Name()).str();
  }

  try {
    int remaining = model_.Buy(player_id, item);
    return (boost::format("Purchased %s for %d gold. %d gold remaining.") % item.Name() % item.MarketPrice() % remaining).str();
  }
  catch (const model::character::InsufficientFunds& e) {
    return "You do not have enough gold!";
  }
}

std::string AccountCommand::Perform(int player_id, const std::vector<std::string>& args) {
  if (args.size() > 1) {
    return "Error: Too many arguments.";
  }

  return Account(player_id);
}

std::string AccountCommand::Account(int player_id) const {
  const auto& player = model_.Player(player_id);

  return (boost::format("You currently have %d gold.") % player.Gold()).str();
}

std::string QuitCommand::Perform(int player_id, const std::vector<std::string>& args) {
  if (args.size() > 1) {
    return "Error: Too many arguments.";
  }

  return Quit(player_id);
}

std::string QuitCommand::Quit(int player_id) {
  auto& player = model_.Player(player_id);
  model_.Logout(player_id);

  return (boost::format("%s logged out.") % player.Name()).str();
}

}  // namespace controller

