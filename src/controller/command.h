/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_CONTROLLER_COMMAND_H_
#define MUDGAME_SRC_CONTROLLER_COMMAND_H_

#include <string>

#include <boost/format.hpp>

#include "enum_information.h"
#include "../utility.h"
#include "../model/location/room.h"
#include "../model/model.h"
#include "../model/exception.h"
#include "../model/character/character_exception.h"
#include "../model/character/player.h"
#include "../model/character/shopkeeper.h"

namespace controller {

class Command {
 public:
  // Constructors
  explicit Command(model::Model& model) : model_(model) {}

  // Destructors
  virtual ~Command(void) {}

  // Accessors
  virtual const std::vector<std::string>& ActivationAliases(void) const = 0;

  // Operations
  virtual std::string Perform(int player_id,
                                     const std::vector<std::string>& args) = 0;

 protected:
  // Attributes
  model::Model& model_;
};

class LookCommand : public Command {
 public:
  // Constructors
  explicit LookCommand(model::Model& model) : Command(model) {}

  // Accessors
  inline const std::vector<std::string>& ActivationAliases(void) const {
    return kActivationAliases;
  }

  // Operations
  std::string Perform(int player_id, const std::vector<std::string>& args);

 private:
  // Attributes
  const std::vector<std::string> kActivationAliases {"look"};

  // Operations
  std::string LookRoom(int player_id) const;
  std::string LookRoom(int player_id, const DirectionInformation& direction_information) const;
  std::string LookItem(int player_id, const model::item::Item& item) const;
};

class OpenCommand : public Command {
 public:
  // Constructors
  explicit OpenCommand(model::Model& model) : Command(model) {}
  
  // Accessors
  inline const std::vector<std::string>& ActivationAliases(void) const {
    return kActivationAliases;
  }

  // Operations
  std::string Perform(int player_id, const std::vector<std::string>& args);

 private:
  // Attributes
  const std::vector<std::string> kActivationAliases {"open"};

  // Operations
  std::string Open(int player_id, const DirectionInformation& direction_information);
};

class CloseCommand : public Command {
 public:
  // Constructors
  explicit CloseCommand(model::Model& model) : Command(model) {}

  // Accessors
  inline const std::vector<std::string>& ActivationAliases(void) const {
    return kActivationAliases;
  }

  // Operations
  std::string Perform(int player_id, const std::vector<std::string>& args);

 private:
  // Attributes
  const std::vector<std::string> kActivationAliases {"close"};

  // Operations
  std::string Close(int player_id, const DirectionInformation& direction_information);
};

class WearCommand : public Command {
 public:
  // Constructors
  explicit WearCommand(model::Model& model) : Command(model) {}

  // Accessors
  inline const std::vector<std::string>& ActivationAliases(void) const {
    return kActivationAliases;
  }

  // Operations
  std::string Perform(int player_id, const std::vector<std::string>& args);

 private:
  // Attributes
  const std::vector<std::string> kActivationAliases {"wear"};

  // Operations
  std::string Wear(int player_id, const std::string& item_name);
};

class RemoveCommand : public Command {
 public:
  // Constructors
  explicit RemoveCommand(model::Model& model) : Command(model) {}
  
  // Accessors
  inline const std::vector<std::string>& ActivationAliases(void) const {
    return kActivationAliases;
  }

  // Operations
  std::string Perform(int player_id, const std::vector<std::string>& args);

 private:
  // Attributes
  const std::vector<std::string> kActivationAliases {"remove"};

  // Operations
  std::string Remove(int player_id, const std::string& item_name);
};

class MoveCommand : public Command {
 public:
  // Constructors
  explicit MoveCommand(model::Model& model) : Command(model) {}

  // Accessors
  inline const std::vector<std::string>& ActivationAliases(void) const {
    return kActivationAliases;
  }

  // Operations
  std::string Perform(int player_id, const std::vector<std::string>& args);

 private:
  // Attributes
  const std::vector<std::string> kActivationAliases {"north", "south", "east", "west", "up", "down"};

  // Operations
  std::string Move(int player_id, const DirectionInformation& direction_information);
};

class ListCommand : public Command {
 public:
  // Constructors
  explicit ListCommand(model::Model& model) : Command(model) {}
  
  // Accessors
  inline const std::vector<std::string>& ActivationAliases(void) const {
    return kActivationAliases;
  }

  // Operations
  std::string Perform(int player_id, const std::vector<std::string>& args);

 private:
  // Attributes
  const std::vector<std::string> kActivationAliases {"list"};

  // Operations
  std::string List(int player_id) const;
};

class BuyCommand : public Command {
 public:
  // Constructors
  explicit BuyCommand(model::Model& model) : Command(model) {}

  // Accessors
  inline const std::vector<std::string>& ActivationAliases(void) const {
    return kActivationAliases;
  }

  // Operations
  std::string Perform(int player_id, const std::vector<std::string>& args);

 private:
  // Attributes
  const std::vector<std::string> kActivationAliases {"buy"};

  // Operations
  std::string Buy(int player_id, const std::string& item_name);
};

class AccountCommand : public Command {
 public:
  // Constructors
  explicit AccountCommand(model::Model& model) : Command(model) {}

  // Accessors
  inline const std::vector<std::string>& ActivationAliases(void) const {
    return kActivationAliases;
  }

  // Operations
  std::string Perform(int player_id, const std::vector<std::string>& args);

 private:
  // Attributes
  const std::vector<std::string> kActivationAliases {"account"};

  // Operations
  std::string Account(int player_id) const;
};

class QuitCommand : public Command {
 public:
  // Constructors
  explicit QuitCommand(model::Model& model) : Command(model) {}

  // Accessors
  inline const std::vector<std::string>& ActivationAliases(void) const {
    return kActivationAliases;
  }

  // Operations
  std::string Perform(int player_id, const std::vector<std::string>& args);

 private:
  // Attributes
  const std::vector<std::string> kActivationAliases {"quit"};

  // Operations
  std::string Quit(int player_id);
};

}  // namespace controller

#endif  // MUDGAME_SRC_CONTROLLER_COMMAND_H_
