/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "controller.h"

namespace controller {

Controller::Controller(const game::GamePaths& game_paths) : kGamePaths(game_paths) {
  model_ = std::make_unique<model::Model>();
  view_ = std::make_unique<view::View>(*this);
  last_tick_ms_ = utility::Utility::CurrentTimeMs();

  try {
    if (kGamePaths.load_as_ascii_) {
      LoadAsciiData(kGamePaths.load_path_);
    } else {
      LoadBinaryData(kGamePaths.load_path_);
    }
  }
  catch (...) {
    throw;
  }

  MakeCommands();
}

void Controller::Start(void) {
  game_running_ = true;
  bool logged_in;
  do {
    logged_in = Login();
  } while (!logged_in);

  GameLoop();
}

void Controller::Quit(void) {
  if (!kGamePaths.ascii_save_directory_.empty()) {
    SaveAsciiData(kGamePaths.ascii_save_directory_);
  }
  if (!kGamePaths.binary_save_file_.empty()) {
    SaveBinaryData(kGamePaths.binary_save_file_);
  }
  view_->SendOutput("Exiting game.");
}

void Controller::GameLoop(void) {
  std::string input;
  while (game_running_) {
    // view_->SendOutput("Enter command: ", false);
    input = view_->GetInput();
    PerformCommand(input);
    GameTick();
  }
  Quit();
}

void Controller::PerformCommand(const std::string& input) {
  auto args = utility::Utility::Split(input, ' ');

  std::string output;
  if (args.size() > 0) {
    for (const auto& command : commands_) {
      for (const auto& alias : command->ActivationAliases()) {
        if (alias == utility::Utility::Lower(args[0])) {
          // view_->SendOutput("Command: " + alias);
          output = command->Perform(player_id_, args);

          const QuitCommand* quit_command  = dynamic_cast<const QuitCommand*>(&(*command));
          if (quit_command != nullptr) {
            // This shuts down the game when the user logs out, or "quits"
            game_running_ = false;
          }
          break;
        }
      }
      if (!output.empty()) {
        break;
      }
    }
  }

  if (output.empty()) {
    output = "Error: Unknown command.";
  }
  view_->SendOutput(output);
}

void Controller::LoadAsciiData(const std::string& load_path) {
  std::string temp_load_path = load_path;
  if (temp_load_path.back() != '/') {
    temp_load_path += '/';
  }

  AsciiLoader ascii_loader(*model_);
  try {
    ascii_loader.LoadItems(temp_load_path + kItemsFileName);
    ascii_loader.LoadAreas(temp_load_path + kAreasFileName);
    ascii_loader.LoadRooms(temp_load_path + kRoomsFileName);
    ascii_loader.LoadConnections(temp_load_path + kConnectionsFileName);
    ascii_loader.LoadPlayers(temp_load_path + kPlayersFileName);
    LoadNpcs();
  }
  catch (...) {
    throw;
  }
}

void Controller::LoadBinaryData(const std::string& load_path) {
  // TODO bonus marks
}

void Controller::MakeCommands(void) {
  commands_.push_back(std::make_unique<LookCommand>(*model_));
  commands_.push_back(std::make_unique<OpenCommand>(*model_));
  commands_.push_back(std::make_unique<CloseCommand>(*model_));
  commands_.push_back(std::make_unique<WearCommand>(*model_));
  commands_.push_back(std::make_unique<RemoveCommand>(*model_));
  commands_.push_back(std::make_unique<MoveCommand>(*model_));
  commands_.push_back(std::make_unique<ListCommand>(*model_));
  commands_.push_back(std::make_unique<BuyCommand>(*model_));
  commands_.push_back(std::make_unique<AccountCommand>(*model_));
  commands_.push_back(std::make_unique<QuitCommand>(*model_));
}

void Controller::SaveAsciiData(const std::string& save_path) {
  std::string temp_save_path = save_path;
  if (temp_save_path.back() != '/') {
    temp_save_path += '/';
  }

  AsciiSaver ascii_saver(*model_);
  try {
    ascii_saver.SaveItems(temp_save_path + kItemsFileName);
    ascii_saver.SaveAreas(temp_save_path + kAreasFileName);
    ascii_saver.SaveRooms(temp_save_path + kRoomsFileName);
    ascii_saver.SaveConnections(temp_save_path + kConnectionsFileName);
    ascii_saver.SavePlayers(temp_save_path + kPlayersFileName);
  }
  catch (...) {
    throw;
  }
}

void Controller::SaveBinaryData(const std::string& save_path) {
  // TODO bonus marks
}

void Controller::LoadNpcs(void) {
  for (const auto& room_pair : model_->Rooms()) {
    const auto& room = room_pair.second;
    if (room->Name() == "The Armory") {
      std::unique_ptr<model::character::Shopkeeper> armorsmith = std::make_unique<model::character::Shopkeeper>("Armorsmith", room->GlobalId());
      for (int armor_id : model_->ArmorIds()) {
//        view_->SendOutput("Added armor " + std::to_string(armor_id));
        armorsmith->AddToWares(armor_id);
      }
      model_->AddNpc(std::move(armorsmith));
    }
    else if (room->Name() == "Weapon Shop") {
      std::unique_ptr<model::character::Shopkeeper> weaponsmith = std::make_unique<model::character::Shopkeeper>("Weaponsmith", room->GlobalId());
      for (int weapon_id : model_->WeaponIds()) {
//        view_->SendOutput("Added weapon " + std::to_string(weapon_id));
        weaponsmith->AddToWares(weapon_id);
      }
      model_->AddNpc(std::move(weaponsmith));
    }
  }
}

bool Controller::Login(void) {
  view_->SendOutput("Enter login details");
  view_->SendOutput("Username: ", false);
  std::string username = view_->GetInput();
  view_->SendOutput("Password: ", false);
  std::string password = view_->GetInput();

  username = utility::Utility::Lower(username);
  unsigned long long password_hash = std::hash<std::string>{}(password);
  model::character::Player* player_ptr = nullptr;
  for (const auto& pair : model_->Players()) {
    auto& player = pair.second;
    if (player->Username() == username) {
      if (model_->Login(pair.first, password_hash)) {
        player_ptr = player.get();
      }
      break;
    }
  }

  if (player_ptr != nullptr && player_ptr->LoggedIn()) {
    player_id_ = player_ptr->Id();
    view_->SendOutput("Welcome " + player_ptr->Name() + "!");
    return true;
  } else {
    view_->SendOutput("Error: Incorrect username/password.");
    return false;
  }
}

void Controller::GameTick(void) {
  auto current = utility::Utility::CurrentTimeMs();
  int count = 0;
  while (current - last_tick_ms_ >= kTickRateMs) {
    model_->GameTick();
    last_tick_ms_ += kTickRateMs;
    ++count;
  }
  if (count == 1) {
    view_->SendOutput("Time ticks...");
  }
  else if (count > 1) {
    view_->SendOutput("Time ticks " + std::to_string(count) + " times..."); 
  }
} 

}  // namespace controller
