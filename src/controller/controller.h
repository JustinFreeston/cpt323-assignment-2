/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_CONTROLLER_CONTROLLER_H_
#define MUDGAME_SRC_CONTROLLER_CONTROLLER_H_

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "ascii_loader.h"
#include "ascii_saver.h"
#include "command.h"
#include "../game/game_paths.h"
#include "../model/model.h"
#include "../view/view.h"

namespace controller {

class Controller {
 public:
  // Constructors
  explicit Controller(const game::GamePaths& game_paths);

  // Operations
  void Start(void);

 private:
  // Attributes
  const std::string kItemsFileName = "items.txt";
  const std::string kAreasFileName = "areas.txt";
  const std::string kRoomsFileName = "rooms.txt";
  const std::string kConnectionsFileName = "exits.txt";
  const std::string kPlayersFileName = "players.txt";
  const game::GamePaths kGamePaths;
  const unsigned kTickRateMs = 60000;
  std::unique_ptr<model::Model> model_;
  std::unique_ptr<view::View> view_;
  std::vector<std::unique_ptr<Command>> commands_;
  int player_id_;
  bool game_running_;
  unsigned long last_tick_ms_;

  // Operations
  void LoadAsciiData(const std::string& load_path);
  void LoadBinaryData(const std::string& load_path);
  void MakeCommands(void);
  void SaveAsciiData(const std::string& save_path);
  void SaveBinaryData(const std::string& save_path);
  void LoadNpcs(void);
  bool Login(void);
  void GameLoop(void);
  void PerformCommand(const std::string& input);
  void Quit(void);
  void GameTick(void);
};

}  // namespace controller

#endif  // MUDGAME_SRC_CONTROLLER_CONTROLLER_H_
