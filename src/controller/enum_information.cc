/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "enum_information.h"

namespace controller {

// Defining static variables in source
std::vector<std::unique_ptr<WearLocationInformation>> EnumInformation::wear_location_information_;
std::vector<std::unique_ptr<DirectionInformation>> EnumInformation::direction_information_;

const WearLocationInformation& EnumInformation::GetWearLocationInformation(model::item::WearLocation wear_location) {
  if (wear_location_information_.empty()) {
    InitWearLocationInformation();
  }

  for (auto const& info : wear_location_information_) {
    if (info->GetWearLocation() == wear_location) {
      return *info;
    }
  }
  throw model::NoSuchElement();
}

const WearLocationInformation& EnumInformation::GetWearLocationInformation(const std::string& string, bool case_sensitive, bool partial_match) {
  if (wear_location_information_.empty()) {
    InitWearLocationInformation();
  }

  std::string temp_string = string;
  if (!case_sensitive) {
    temp_string = utility::Utility::Upper(temp_string);
  }

  for (auto const& info : wear_location_information_) {
    std::string loop_string = info->GetString();
    if (!case_sensitive) {
      loop_string = utility::Utility::Upper(loop_string);
    }
    if (partial_match && utility::Utility::StartsWith(loop_string, temp_string)) {
      return *info;
    }
    else if (loop_string == temp_string) {
      return *info;
    }
  }
  throw model::NoSuchElement();
}

const WearLocationInformation& EnumInformation::GetWearLocationInformation(int value) {
  if (wear_location_information_.empty()) {
    InitWearLocationInformation();
  }

  for (auto const& info : wear_location_information_) {
    if (value == static_cast<int>(info->GetWearLocation())) {
      return *info;
    }
  }
  throw model::NoSuchElement();
}

const DirectionInformation& EnumInformation::GetDirectionInformation(model::location::Direction direction) {
  if (direction_information_.empty()) {
    InitDirectionInformation();
  }

  for (auto const& info : direction_information_) {
    if (info->GetDirection() == direction) {
      return *info;
    }
  }
  throw model::NoSuchElement();
}

const DirectionInformation& EnumInformation::GetDirectionInformation(const std::string& string, bool case_sensitive, bool partial_match) {
  if (direction_information_.empty()) {
    InitDirectionInformation();
  }

  std::string temp_string = string;
  if (!case_sensitive) {
    temp_string = utility::Utility::Upper(temp_string);
  }

  for (const auto& info : direction_information_) {
    std::string loop_string = info->GetString();
    if (partial_match && utility::Utility::StartsWith(loop_string, temp_string)) {
      return *info;
    }
    else if (loop_string == temp_string) {
      return *info;
    }
  }
  throw model::NoSuchElement();
}

const DirectionInformation& EnumInformation::GetDirectionInformation(int value) {
  if (direction_information_.empty()) {
    InitDirectionInformation();
  }

  for (auto const& info : direction_information_) {
    if (value == static_cast<int>(info->GetDirection())) {
      return *info;
    }
  }
  throw model::NoSuchElement();
}

void EnumInformation::InitWearLocationInformation(void) {
  wear_location_information_.push_back(std::make_unique<WearLocationInformation>(model::item::WearLocation::kWrists, "WRISTS"));
  wear_location_information_.push_back(std::make_unique<WearLocationInformation>(model::item::WearLocation::kArms, "ARMS"));
  wear_location_information_.push_back(std::make_unique<WearLocationInformation>(model::item::WearLocation::kHands, "HANDS"));
  wear_location_information_.push_back(std::make_unique<WearLocationInformation>(model::item::WearLocation::kLegs, "LEGS"));
  wear_location_information_.push_back(std::make_unique<WearLocationInformation>(model::item::WearLocation::kFeet, "FEET"));
  wear_location_information_.push_back(std::make_unique<WearLocationInformation>(model::item::WearLocation::kHead, "HEAD"));
  wear_location_information_.push_back(std::make_unique<WearLocationInformation>(model::item::WearLocation::kTorso, "TORSO"));
  wear_location_information_.push_back(std::make_unique<WearLocationInformation>(model::item::WearLocation::kShield, "SHIELD"));
}

void EnumInformation::InitDirectionInformation(void) {
  direction_information_.push_back(std::make_unique<DirectionInformation>(model::location::Direction::kNorth, "NORTH"));
  direction_information_.push_back(std::make_unique<DirectionInformation>(model::location::Direction::kSouth, "SOUTH"));
  direction_information_.push_back(std::make_unique<DirectionInformation>(model::location::Direction::kEast, "EAST"));
  direction_information_.push_back(std::make_unique<DirectionInformation>(model::location::Direction::kWest, "WEST"));
  direction_information_.push_back(std::make_unique<DirectionInformation>(model::location::Direction::kUp, "UP"));
  direction_information_.push_back(std::make_unique<DirectionInformation>(model::location::Direction::kDown, "DOWN"));
}

}  // namespace controller
