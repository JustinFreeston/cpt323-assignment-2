/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_CONTROLLER_STRING_TO_ENUM_H_
#define MUDGAME_SRC_CONTROLLER_STRING_TO_ENUM_H_

#include <memory>
#include <iostream> // TODO REMOVE
#include <string>
#include <vector>

#include "../utility.h"
#include "../model/exception.h"
#include "../model/item/item.h"
#include "../model/location/room.h"

namespace controller {

class WearLocationInformation {
 public:
  // Constructors
  WearLocationInformation(model::item::WearLocation wear_location, const std::string& string) : kWearLocation(wear_location), kString(string), kValue(static_cast<int>(wear_location)) {}

  // Accessors
  inline model::item::WearLocation GetWearLocation(void) const {
    return kWearLocation;
  }
  inline const std::string& GetString(void) const {
    return kString;
  }
  inline int GetValue(void) const {
    return kValue;
  }

 private:
  // Attributes
  const model::item::WearLocation kWearLocation;
  const std::string kString;
  const int kValue;
};

class DirectionInformation {
 public:
  // Constructors
  DirectionInformation(model::location::Direction direction, const std::string& string) : kDirection(direction), kString(string), kValue(static_cast<int>(direction)) {}

  // Accessors
  inline model::location::Direction GetDirection(void) const {
    return kDirection;
  }
  inline const std::string& GetString(void) const {
    return kString;
  }
  inline int GetValue(void) const {
    return kValue;
  }

 private:
  // Attributes
  const model::location::Direction kDirection;
  const std::string kString;
  const int kValue;
};

class EnumInformation {
 public:
  // Constructors
  EnumInformation(void) {}

  // Operations
  static const WearLocationInformation& GetWearLocationInformation(model::item::WearLocation wear_location);
  static const WearLocationInformation& GetWearLocationInformation(const std::string& string, bool case_sensitive, bool partial_match);
  static const WearLocationInformation& GetWearLocationInformation(int value);
  static const DirectionInformation& GetDirectionInformation(model::location::Direction direction);
  static const DirectionInformation& GetDirectionInformation(const std::string& string, bool case_sensitive, bool partial_match);
  static const DirectionInformation& GetDirectionInformation(int value);

 private:
  // Attributes
  static std::vector<std::unique_ptr<WearLocationInformation>> wear_location_information_;
  static std::vector<std::unique_ptr<DirectionInformation>> direction_information_;

  // Operations
  static void InitWearLocationInformation(void);
  static void InitDirectionInformation(void);
};

}  // namespace controller

#endif  // MUDGAME_SRC_CONTROLLER_STRING_TO_ENUM_H_
