/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "game.h"

namespace game {

Game::Game(const CommandLineArgs& cla) {
  game_paths_ = std::make_unique<game::GamePaths>();
  if (!cla.AsciiLoad().empty() && cla.BinaryLoad().empty()) {
    if (boost::filesystem::is_directory(cla.AsciiLoad())) {
      game_paths_->load_as_ascii_ = true;
      game_paths_->load_path_ = cla.AsciiLoad();
    } else {
      throw std::invalid_argument("ascii_load must be a directory");
    }
  } else if (!cla.BinaryLoad().empty() && cla.AsciiLoad().empty()) {
    if (boost::filesystem::is_regular_file(cla.BinaryLoad())) {
      game_paths_->load_as_ascii_ = false;
      game_paths_->load_path_ = cla.BinaryLoad();
    } else {
      throw std::invalid_argument("binary_load must be file");
    }
  } else {
    // Neither ascii_load or binary_load were set or both were set
    throw std::invalid_argument("Exactly one of either ascii_load or " \
                                "binary_load must be set");
  }

  if (!cla.AsciiSave().empty()) {
    if (boost::filesystem::is_directory(cla.AsciiSave())) {
      SetAsciiSaveDirectory(cla.AsciiSave());
    } else {
      throw std::invalid_argument("ascii_save must be a directory or ignored");
    }
  }
  if (!cla.BinarySave().empty()) {
    if (boost::filesystem::is_regular_file(cla.BinarySave())) {
      SetBinarySaveFile(cla.BinarySave());
    } else {
      throw std::invalid_argument("binary_save must be a file or ignored");
    }
  }
  controller_ = nullptr;
}

}  // namespace game
