/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_GAME_GAME_H_
#define MUDGAME_SRC_GAME_GAME_H_

#include <exception>
#include <stdexcept>
#include <string>
#include <utility>

#include <boost/filesystem.hpp>

#include "game_paths.h"
#include "../command_line_args.h"
#include "../controller/controller.h"

namespace game {

class Game {
 public:
  // Constructors
  explicit Game(const CommandLineArgs& cla);

  // Mutators
  inline void SetAsciiSaveDirectory(const std::string& ascii_save_directory) {
    game_paths_->ascii_save_directory_ = ascii_save_directory;
  }
  inline void SetBinarySaveFile(const std::string& binary_save_file) {
    game_paths_->binary_save_file_ = binary_save_file;
  }

  // Operations
  inline void Load(void) {
    controller_ = std::make_unique<controller::Controller>(*game_paths_);
  }

  inline void Start(void) {
    if (controller_ == nullptr) {
      throw std::runtime_error("Game has not been loaded");
    }
    controller_->Start();
  }

 private:
  // Attributes
  std::unique_ptr<game::GamePaths> game_paths_;
  std::unique_ptr<controller::Controller> controller_;
};

}  // namespace game

#endif  // MUDGAME_SRC_GAME_GAME_H_
