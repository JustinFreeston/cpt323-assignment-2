/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "character.h"

namespace model {
namespace character {

// Load static variable
int Character::id_count_ = 0;

void Character::EquipWeapon(int weapon_id) {
  if (weapon_id_ != kNullItem) {
    throw WearLocationOccupied("Cannot equip a weapon when one is already equipped");
  }
  weapon_id_ = weapon_id;
}

bool Character::UnequipWeapon(void) {
  if (weapon_id_ == kNullItem) {
    return false;
  }
  weapon_id_ = kNullItem;
  return true;
}

void Character::EquipArmor(int armor_id, item::WearLocation wear_location) {
  int wear_location_value = static_cast<int>(wear_location);
  if (armor_ids_[wear_location_value] != kNullItem) {
    throw WearLocationOccupied("Cannot equip armor in a location where "\
                               "armor is already equipped.");
  }
  armor_ids_[wear_location_value] = armor_id;
}

bool Character::UnequipArmor(item::WearLocation wear_location) {
  int  wear_location_value = static_cast<int>(wear_location);
  if (armor_ids_[wear_location_value] == kNullItem) {
    return false;
  }
  armor_ids_[wear_location_value] = kNullItem;
  return true;
}

}  // namespace character
}  // namespace model

