/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_MODEL_CHARACTER_CHARACTER_H_
#define MUDGAME_SRC_MODEL_CHARACTER_CHARACTER_H_

#include <algorithm>
#include <array>
#include <string>
#include <vector>

#include "character_exception.h"
#include "../item/item.h"

namespace model {
namespace location {
  // Forward declaration
  class Room;
}
namespace character {

const int kNullItem = 0;

class Character {
 public:
  // Constructors
  Character(int id, const std::string& name, int intelligence, int wisdom,
            int dexterity, int strength, int constitution, int hitpoints,
            int mana, int location_id) : kId(id), kName(name),
            kIntelligence(intelligence), kWisdom(wisdom),
            kDexterity(dexterity), kStrength(strength),
            kConstitution(constitution), hitpoints_(hitpoints), mana_(mana),
            location_id_(location_id) {
    armor_ids_.fill(kNullItem);
  }
  
  Character(const std::string& name, int intelligence, int wisdom, int dexterity,
            int strength, int constitution, int hitpoints, int mana,
            int location_id) : Character(++id_count_, name, intelligence, wisdom,
            dexterity, strength, constitution, hitpoints, mana, location_id) {}

  // Destructors
  virtual ~Character(void) {}

  // Accessors
  inline static int IdCount(void) {
    return id_count_;
  }
  inline int Id(void) const {
    return kId;
  }
  inline const std::string& Name(void) const {
    return kName;
  }
  inline int Intelligence(void) const {
    return kIntelligence;
  }
  inline int Wisdom(void) const {
    return kWisdom;
  }
  inline int Dexterity(void) const {
    return kDexterity;
  }
  inline int Strength(void) const {
    return kStrength;
  }
  inline int Constitution(void) const {
    return kConstitution;
  }
  inline int Hitpoints(void) const {
    return hitpoints_;
  }
  inline int Mana(void) const {
    return mana_;
  }
  inline int LocationId(void) const {
    return location_id_;
  }
  inline int WeaponId(void) const {
    return weapon_id_;
  }
  inline const std::array<int, item::kTotalWearLocations> ArmorIds(void) const {
    return armor_ids_;
  }
  inline int ArmorId(item::WearLocation wear_location) const {
    int wear_location_value = static_cast<int>(wear_location);
    return armor_ids_[wear_location_value];
  }
  inline const std::vector<int> InventoryIds(void) const {
    return inventory_ids_;
  }
  inline bool HasInInventory(int item_id) const {
    auto position = std::find(inventory_ids_.begin(), inventory_ids_.end(),
                              item_id);
    if (position == inventory_ids_.end()) {
      return false;
    }
    return true;
  }

  // Mutators
  inline virtual int SetHitpoints(int hitpoints) {
    hitpoints_ = hitpoints;
    return hitpoints_;
  }
  inline virtual int SetMana(int mana) {
    mana_ = mana;
    return mana_;
  }
  inline void SetLocationId(int location_id) {
    location_id_ = location_id;
  }
  virtual void EquipWeapon(int weapon_id);
  virtual bool UnequipWeapon(void);
  virtual void EquipArmor(int armor_id, item::WearLocation wear_location);
  virtual bool UnequipArmor(item::WearLocation wear_location);
  inline void AddToInventory(int item_id) {
    inventory_ids_.push_back(item_id);
  }
  inline bool RemoveFromInventory(int item_id) {
    auto position = std::find(inventory_ids_.begin(), inventory_ids_.end(),
                              item_id);
    if (position == inventory_ids_.end()) {
      return false;
    }
    inventory_ids_.erase(position);
    return true;
  }

 private:
  // Attributes
  static int id_count_;
  const int kId;
  const std::string kName;
  const int kIntelligence;
  const int kWisdom;
  const int kDexterity;
  const int kStrength;
  const int kConstitution;
  int hitpoints_;
  int mana_;
  int location_id_;
  int weapon_id_;
  std::array<int, item::kTotalWearLocations> armor_ids_;
  std::vector<int> inventory_ids_;
};

}  // namespace character
}  // namespace model

#endif  // MUDGAME_SRC_MODEL_CHARACTER_CHARACTER_H_
