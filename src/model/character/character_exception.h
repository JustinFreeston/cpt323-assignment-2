/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_MODEL_CHARACTER_EXCEPTION_H_
#define MUDGAME_SRC_MODEL_CHARACTER_EXCEPTION_H_

#include <stdexcept>

namespace model {
namespace character {

class WearLocationOccupied : public std::runtime_error {
 public:
  explicit WearLocationOccupied(const std::string& what_arg =
           "Wear location occupied by another item") :
           std::runtime_error(what_arg) {}
};

class NotInInventory : public std::runtime_error {
 public:
  explicit NotInInventory(const std::string& what_arg =
           "Item is not in the character's inventory") :
           std::runtime_error(what_arg) {}
};

class PlayerAlreadyLoggedIn : public std::runtime_error {
 public:
  explicit PlayerAlreadyLoggedIn(const std::string& what_arg =
           "The player is already logged in") : std::runtime_error(what_arg) {}
};

class NotLoggedIn : public std::runtime_error {
 public:
  explicit NotLoggedIn(const std::string& what_arg =
           "The player is not logged in") : std::runtime_error(what_arg) {}
};

class InsufficientFunds : public std::runtime_error {
 public:
  explicit InsufficientFunds(const std::string& what_arg =
            "Insufficient funds") : std::runtime_error(what_arg) {}
};

}  // namespace character
}  // namespace model

#endif  // MUDGAME_SRC_MODEL_CHARACTER_EXCEPTION_H_
