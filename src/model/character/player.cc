/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "player.h"

namespace model {
namespace character {

std::string Player::ToString(void) const {
  std::string as_string = std::to_string(Id());
  as_string += kAsciiSeparator;
  as_string += Username();
  as_string += kAsciiSeparator;
  as_string += std::to_string(kPasswordHash);
  as_string += kAsciiSeparator;
  as_string += std::to_string(LocationId());
  for (int armor_id : ArmorIds()) {
    as_string += kAsciiSeparator;
    as_string += std::to_string(armor_id);
  }
  as_string += kAsciiSeparator;
  as_string += std::to_string(WeaponId());
  for (int item_id : InventoryIds()) {
    as_string += kAsciiSeparator;
    as_string += std::to_string(item_id);
  }
  as_string += kAsciiSeparator;
  as_string += std::to_string(Gold());
  as_string += kAsciiSeparator;
  as_string += std::to_string(Intelligence());
  as_string += kAsciiSeparator;
  as_string += std::to_string(Wisdom());
  as_string += kAsciiSeparator;
  as_string += std::to_string(Strength());
  as_string += kAsciiSeparator;
  as_string += std::to_string(Dexterity());
  as_string += kAsciiSeparator;
  as_string += std::to_string(Constitution());
  as_string += kAsciiSeparator;
  as_string += std::to_string(Hitpoints());
  as_string += kAsciiSeparator;
  as_string += std::to_string(Mana());
  as_string += kAsciiSeparator;
  as_string += std::to_string(Moves());

  return as_string;
}

// Operator overloads
std::ostream& operator<<(std::ostream& os, const Player& player) {
  os << player.ToString();
  return os;
}

// TODO Figure out how to implement for this model
std::istream& operator>>(std::istream& is, Player& player) {
  return is;
}

}  // namespace character
}  // namespace model
