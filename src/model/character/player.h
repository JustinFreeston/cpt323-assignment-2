/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_MODEL_CHARACTER_PLAYER_H_
#define MUDGAME_SRC_MODEL_CHARACTER_PLAYER_H_

#include <string>

#include "character.h"
#include "character_exception.h"
#include "../item/item.h"

namespace model {
namespace character {

class Player : public Character {
 public:
  // Constructors
  Player(int id, const std::string& username, unsigned long long password_hash,
         int intelligence, int wisdom, int dexterity, int strength,
         int constitution, int hitpoints, int mana, int location_id, int moves,
         int gold) : Character(id, username, intelligence, wisdom, dexterity,
         strength, constitution, hitpoints, mana, location_id),
         kUsername(username), kPasswordHash(password_hash), logged_in_(false),
         moves_(moves), gold_(gold) {}

  // Accessors
  inline const std::string& Username(void) const {
    return kUsername;
  }
  inline bool LoggedIn(void) const {
    return logged_in_;
  }
  inline int Moves(void) const {
    return moves_;
  }
  inline int Gold(void) const {
    return gold_;
  }
  std::string ToString(void) const;

  // Mutators
  inline virtual int SetHitpoints(int hitpoints) {
    if (hitpoints > kMaxHitpoints) {
      return Hitpoints();
    }
    return Character::SetHitpoints(hitpoints);
  }
  inline virtual int SetMana(int mana) {
    if (mana > kMaxMana) {
      return Mana();
    }
    return Character::SetMana(mana);
  }
  inline int SetMoves(int moves) {
    if (moves <= kMaxMoves) {
      moves_ = moves;
    }
    return moves_;
  }
  inline void SetGold(int gold) {
    gold_ = gold;
  }
  inline bool Login(unsigned long long password_hash) {
    if (logged_in_) {
      throw PlayerAlreadyLoggedIn();
    }
    if (password_hash == kPasswordHash) {
      logged_in_ = true;
      return true;
    }
    return false;
  }
  inline void Logout(void) {
    if (!logged_in_) {
      throw NotLoggedIn();
    }
    logged_in_ = false;
  }
  inline virtual void EquipWeapon(int weapon_id) {
    if (!HasInInventory(weapon_id)) {
      throw NotInInventory();
    }
    try {
      Character::EquipWeapon(weapon_id);
      RemoveFromInventory(weapon_id);
    }
    catch (const WearLocationOccupied& e) {
      throw;
    }
  }
  inline virtual bool UnequipWeapon(void) {
    int weapon_id = WeaponId();
    if (Character::UnequipWeapon()) {
      AddToInventory(weapon_id);
      return true;
    }
    return false;
  }
  inline virtual void EquipArmor(int armor_id,
        item::WearLocation wear_location) {
    if (!HasInInventory(armor_id)) {
      throw NotInInventory();
    }
    try {
      Character::EquipArmor(armor_id, wear_location);
      RemoveFromInventory(armor_id);
    }
    catch (const WearLocationOccupied& e) {
      throw;
    }
  }
  inline virtual bool UnequipArmor(item::WearLocation wear_location) {
    int armor_id = ArmorId(wear_location);
    if (Character::UnequipArmor(wear_location)) {
      AddToInventory(armor_id);
      return true;
    }
    return false;
  }
  inline int Buy(const item::Item& item) {
    int market_price = item.MarketPrice();
    if (market_price > gold_) {
      throw InsufficientFunds("Cannot afford to buy item");
    }
    
    gold_ -= market_price;
    AddToInventory(item.Id());
    return gold_;
  }

 private:
  // Attributes
  const std::string kAsciiSeparator = "\t";
  const int kMaxHitpoints = 25;
  const int kMaxMana = 25;
  const int kMaxMoves = 25;
  const std::string kUsername;
  const unsigned long long kPasswordHash;
  bool logged_in_;
  int moves_;
  int gold_;
};

}  // namespace character
}  // namespace model

#endif  // MUDGAME_SRC_MODEL_CHARACTER_PLAYER_H_
