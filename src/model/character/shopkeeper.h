/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_MODEL_CHARACTER_SHOPKEEPER_H_
#define MUDGAME_SRC_MODEL_CHARACTER_SHOPKEEPER_H_

#include <algorithm>
#include <string>
#include <vector>

#include "character.h"
#include "character_exception.h"

namespace model {
namespace character {

class Shopkeeper : public Character {
 public:
  // Constructor
  Shopkeeper(const std::string& name, int location_id) :
             Character(name, 0, 0, 0, 0, 0, 0, 0, location_id) {}

  // Accessors
  inline const std::vector<int> WareIds(void) const {
    return ware_ids_;
  }
  bool HasWare(int item_id) const;

  // Mutators
  inline void AddToWares(int item_id) {
    ware_ids_.push_back(item_id);
  }
  bool RemoveFromWares(int item_id);

 private:
  // Attributes
  std::vector<int> ware_ids_;
};

}  // namespace character
}  // namespace model

#endif  // MUDGAME_SRC_MODEL_CHARACTER_SHOPKEEPER_H_
