/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "item.h"

namespace model {
namespace item {

std::string Item::ToString(void) const {
  std::string wear_location_field = "NOT_EQUIPABLE";
  const Armor* armor = dynamic_cast<const Armor*>(this);
  if (armor != nullptr) {
    // Errors with this one as well and no time to fix
    // const auto& wear_location_information = controller::EnumInformation::GetWearLocationInformation(armor->GetWearLocation());
    // wear_location_field = wear_location_information.GetString();
    wear_location_field = "TEMP_ARMOR";
  } else {
    const Weapon* weapon = dynamic_cast<const Weapon*>(this);
    if (weapon != nullptr) {
      wear_location_field = "WEAPON";
    }
  }

  std::string as_string = std::to_string(Id());
  as_string += kAsciiSeparator;
  as_string += wear_location_field;
  as_string += kAsciiSeparator;
  as_string += Name();
  as_string += kAsciiSeparator;
  as_string += Description();
  as_string += kAsciiSeparator;
  as_string += std::to_string(MarketPrice());

  return as_string;
}

// Operator overloads
std::ostream& operator<<(std::ostream& os, const Item& item) {
  os << item.ToString();
  return os;
}

// TODO Figure out how to implement for this model
std::istream& operator>>(std::istream& is, Item& item) {
  return is;
}

}  // namespace model
}  // namespace item
