/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_MODEL_ITEM_ITEM_H_
#define MUDGAME_SRC_MODEL_ITEM_ITEM_H_

#include <string>

// Yeah still can't figure it out
// Forward declaration
// namespace controller {
// class EnumInformation;
// }  // namespace controller

namespace model {
namespace item {

const int kTotalWearLocations = 8;

enum class WearLocation {
  kWrists,
  kArms,
  kHands,
  kLegs,
  kFeet,
  kHead,
  kTorso,
  kShield,
};

class Item {
 public:
  // Constructors
  Item(int id, const std::string& name, const std::string& description,
       int market_price) : kId(id), kName(name), kDescription(description),
       kMarketPrice(market_price) {}

  // Destructors
  virtual ~Item(void) {}

  // Accessors
  inline int Id(void) const {
    return kId;
  }
  inline const std::string& Name(void) const {
    return kName;
  }
  inline const std::string& Description(void) const {
    return kDescription;
  }
  inline int MarketPrice(void) const {
    return kMarketPrice;
  }
  std::string ToString(void) const;

 private:
  // Attributes
  const std::string kAsciiSeparator = "\t";
  const int kId;
  const std::string kName;
  const std::string kDescription;
  const int kMarketPrice;
};

class Weapon : public Item {
 public:
  // Constructors
  Weapon(int id, const std::string& name, const std::string& description,
         int market_price) : Item(id, name, description, market_price) {}
};

class Armor : public Item {
 public:
  // Constructors
  Armor(int id, const std::string& name, const std::string& description,
        int market_price, WearLocation wear_location) :
        Item(id, name, description, market_price), kWearLocation(wear_location)
        {}

  // Accessors
  inline WearLocation GetWearLocation(void) const {
    return kWearLocation;
  }

 private:
  // Attributes
  const WearLocation kWearLocation;
};

}  // namespace item
}  // namespace model

#endif  // MUDGAME_SRC_MODEL_ITEM_ITEM_H_
