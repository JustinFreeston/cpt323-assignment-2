/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "room.h"

namespace model {
namespace location {

void Room::RemoveNpc(int npc_id) {
  auto position = std::find(npc_ids_.begin(), npc_ids_.end(),
                            npc_id);
  if (position == npc_ids_.end()) {
    throw NoSuchElement();
  }
  npc_ids_.erase(position);
}

void Room::RemovePlayer(int player_id) {
  auto position = std::find(player_ids_.begin(), player_ids_.end(),
                            player_id);
  if (position == player_ids_.end()) {
    throw NoSuchElement();
  }
  player_ids_.erase(position);
}

std::string Room::ToString(void) const {
  std::string as_string = std::to_string(GlobalId());
  as_string += kAsciiSeparator;
  as_string += std::to_string(LocalId());
  as_string += kAsciiSeparator;
  as_string += std::to_string(AreaId());
  as_string += kAsciiSeparator;
  as_string += Name();
  as_string += kAsciiSeparator;
  as_string += Description();

  return as_string;
}

// Operator overloads
std::ostream& operator<<(std::ostream& os, const Room& room) {
  os << room.ToString();
  return os;
}

// TODO Figure out how to implement for this model
std::istream& operator>>(std::istream& is, Room& room) {
  return is;
}

// Defining static variables in source
int Connection::id_count_ = 0;

std::string Connection::ToString(void) const {
  // TODO Can't figure out errors even with forward declaration
  // const auto& direction_information = controller::EnumInformation::GetDirectionInformation(GetDirection());
  std::string as_string = std::to_string(OriginId());
  as_string += kAsciiSeparator;
  as_string += std::to_string(DestinationId());
  as_string += kAsciiSeparator;
  // as_string += direction_information.GetString();
  as_string += "TEMP_DIRECTION";
  as_string += kAsciiSeparator;
  if (Open()) {
    as_string += "true";
  } else {
    as_string += "false";
  }

  return as_string;
}

// Operator overloads
std::ostream& operator<<(std::ostream& os, const Connection& connection) {
  os << connection.ToString();
  return os;
}

// TODO Figure out how to implement for this model
std::istream& operator>>(std::istream& is, Connection& connection) {
  return is;
}

}  // namespace location
}  // namespace model

