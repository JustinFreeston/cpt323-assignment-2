/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_MODEL_LOCATION_ROOM_H_
#define MUDGAME_SRC_MODEL_LOCATION_ROOM_H_

#include <algorithm>
#include <array>
#include <string>
#include <vector>

#include "../exception.h"

// Forward declaration, can't figure out errors
// namespace controller {
// class EnumInformation;
// }  // namespace controller

namespace model {
namespace location {

const int kNullConnection = 0;
const int kTotalDirections = 6;

enum class Direction {
  kNorth,
  kSouth,
  kEast,
  kWest,
  kUp,
  kDown,
};

class Room {
 public:
  // Constructors
  Room(int global_id, int local_id, int area_id, const std::string& name,
       const std::string& description) : kGlobalId(global_id),
       kLocalId(local_id), kAreaId(area_id), kName(name),
       kDescription(description) {
    connection_ids_.fill(kNullConnection);
  }

  // Accessors
  inline int GlobalId(void) const {
    return kGlobalId;
  }
  inline int LocalId(void) const {
    return kLocalId;
  }
  inline int AreaId(void) const {
    return kAreaId;
  }
  inline const std::string& Name(void) const {
    return kName;
  }
  inline const std::string& Description(void) const {
    return kDescription;
  }
  inline const std::array<int, kTotalDirections> ConnectionIds(void) const {
    return connection_ids_;
  }
  inline int ConnectionId(Direction direction) const {
    int connection_id = connection_ids_[static_cast<int>(direction)];
    if (connection_id == kNullConnection) {
      throw NoSuchElement();
    }
    return connection_id;
  }
  inline const std::vector<int> NpcIds(void) const {
    return npc_ids_;
  }
  inline const std::vector<int> PlayerIds(void) const {
    return player_ids_;
  }
  std::string ToString(void) const;

  // Mutators
  inline void AddConnection(int connection_id, Direction direction) {
    connection_ids_[static_cast<int>(direction)] = connection_id;
  }
  inline void RemoveConnection(Direction direction) {
    int direction_value = static_cast<int>(direction);
    if (connection_ids_[direction_value] == kNullConnection) {
      throw NoSuchElement();
    }
    connection_ids_[direction_value] = kNullConnection;
  }
  inline void AddNpc(int npc_id) {
    npc_ids_.push_back(npc_id);
  }
  void RemoveNpc(int npc_id);
  inline void AddPlayer(int player_id) {
    player_ids_.push_back(player_id);
  }
  void RemovePlayer(int player_id);

 private:
  // Attributes
  const std::string kAsciiSeparator = "\t";
  const int kGlobalId;
  const int kLocalId;
  const int kAreaId;
  const std::string kName;
  const std::string kDescription;
  std::array<int, kTotalDirections> connection_ids_;
  std::vector<int> npc_ids_;
  std::vector<int> player_ids_;
};

class Connection {
 public:
  // Constructors
  Connection(int origin_id, int destination_id, Direction direction,
             bool open) : kId(++id_count_), kOriginId(origin_id),
             kDestinationId(destination_id), kDirection(direction), open_(open)
             {}

  // Accessors
  inline static int IdCount(void) {
    return id_count_;
  }
  inline int Id(void) const {
    return kId;
  }
  inline int OriginId(void) const {
    return kOriginId;
  }
  inline int DestinationId(void) const {
    return kDestinationId;
  }
  inline Direction GetDirection(void) const {
    return kDirection;
  }
  inline bool Open(void) const {
    return open_;
  }
  std::string ToString(void) const;

  // Mutators
  inline bool SetOpen(bool open) {
    if (open_ == open) {
      return false;
    }
    open_ = open;
    return true;
  }

 private:
  // Attributes
  const std::string kAsciiSeparator = "\t";
  static int id_count_;
  const int kId;
  const int kOriginId;
  const int kDestinationId;
  const Direction kDirection;
  bool open_;
};

}  // namespace location
}  // namespace model

#endif  // MUDGAME_SRC_MODEL_LOCATION_ROOM_H_
