/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "model.h"

namespace model {

void Model::AddItem(std::unique_ptr<item::Item>&& item) {
  if (items_.count(item->Id())) {
    throw DuplicateObject("Duplicate item with id " + item->Id());
  }
  items_[item->Id()] = std::move(item);
}

void Model::AddArea(std::unique_ptr<location::Area>&& area) {
  if (areas_.count(area->Id())) {
    throw DuplicateObject("Duplicate area with id " + area->Id());
  }
  areas_[area->Id()] = std::move(area);
}

void Model::AddRoom(std::unique_ptr<location::Room>&& room) {
  if (rooms_.count(room->GlobalId())) {
    throw DuplicateObject("Duplicate room with id " + room->GlobalId());
  }
  int local_id = room->LocalId();
  int area_id = room->AreaId();
  for (const auto& pair : Rooms()) {
    const auto& temp_room = pair.second;
    if (temp_room->LocalId() == local_id && temp_room->AreaId() == area_id) {
      throw DuplicateObject("Duplicate room with local_id and area_id, id " + room->GlobalId());
    }
  }
  rooms_[room->GlobalId()] = std::move(room);
}

void Model::AddConnection(std::unique_ptr<location::Connection>&& connection) {
  int connection_id = connection->Id();
  int origin_id = connection->OriginId();
  int destination_id = connection->DestinationId();
  if (!rooms_.count(origin_id)) {
    throw NoSuchElement("Connection id " + std::to_string(connection_id) + " references invalid origin room id " + std::to_string(origin_id));
  }
  if (!rooms_.count(destination_id)) {
    throw NoSuchElement("Connection id " + std::to_string(connection_id) + " references invalid destination room id " + std::to_string(destination_id));
  }
  auto& origin_room = rooms_.at(origin_id);
  auto& destination_room = rooms_.at(destination_id);
  auto direction = connection->GetDirection();
  origin_room->AddConnection(connection_id, direction);

  // Figure out the opposite direction
  const auto& direction_information = controller::EnumInformation::GetDirectionInformation(direction);
  int direction_value = direction_information.GetValue();
  int opposite_direction_value;
  // If direction % 2 == 0, opposite is +1, otherwise -1
  if (direction_value % 2 == 0) {
    opposite_direction_value = direction_value + 1;
  } else {
    opposite_direction_value = direction_value - 1;
  }
  const auto& opposite_direction_information = controller::EnumInformation::GetDirectionInformation(opposite_direction_value);
  destination_room->AddConnection(connection_id, opposite_direction_information.GetDirection());

  connections_[connection->Id()] = std::move(connection);
}

void Model::AddNpc(std::unique_ptr<character::Character>&& npc) {
  int npc_id = npc->Id();
  if (npcs_.count(npc_id)) {
    throw DuplicateObject("Duplicate npc with id " + npc_id);
  }
  int location_id = npc->LocationId();
  if (!rooms_.count(location_id)) {
    throw NoSuchElement("Npc with id " + std::to_string(npc_id) + " is in a room with invalid id " + std::to_string(location_id));
  }

  auto& room = rooms_.at(location_id);
  room->AddNpc(npc_id);

  npcs_[npc->Id()] = std::move(npc);
}

void Model::AddPlayer(std::unique_ptr<character::Player>&& player) {
  int player_id = player->Id();
  if (players_.count(player_id)) {
    throw DuplicateObject("Duplicate player with id " + player_id);
  }
  if (player->LocationId() < 1) {
    player->SetLocationId(kDefaultPlayerSpawn);
  }
  int location_id = player->LocationId();
  if (!rooms_.count(location_id)) {
    throw NoSuchElement("Player with id " + std::to_string(player_id) + " is in room with invalid id " + std::to_string(location_id));
  }

  auto& room = rooms_.at(location_id);
  room->AddPlayer(player_id);

  players_[player_id] = std::move(player);
}

void Model::GameTick(void) {
  for (auto& pair : players_) {
    auto& player = pair.second;
    if (player->LoggedIn()) {
      int hitpoints_increase = player->Constitution() / 10;
      int mana_increase = player->Wisdom() / 10;
      int moves_increase = player->Dexterity() / 10;
//    std::cout << "Increasing " + player->Name() + " hp by " + std::to_string(hitpoints_increase) << std::endl;
//    std::cout << "Increasing " + player->Name() + " mp by " + std::to_string(mana_increase) << std::endl;
//    std::cout << "Increasing " + player->Name() + " mov by " + std::to_string(moves_increase) << std::endl;
      player->SetHitpoints(player->Hitpoints() + hitpoints_increase);
      player->SetMana(player->Mana() + mana_increase);
      player->SetMoves(player->Moves() + moves_increase);
    }
  }
}

const std::vector<item::Item*> Model::FindItemsByName(const std::string&
                                                      item_name) const {
  std::string item_name_lower = item_name;
  std::transform(item_name_lower.begin(), item_name_lower.end(),
        item_name_lower.begin(), ::tolower);
  std::vector<item::Item*> found_items;

  for (const auto& pair : items_) {
    auto& item = pair.second;
    std::string temp_item_name_lower = item->Name();
    std::transform(temp_item_name_lower.begin(), temp_item_name_lower.end(),
          temp_item_name_lower.begin(), ::tolower);
    if (utility::Utility::StartsWith(temp_item_name_lower, item_name_lower)) {
      found_items.push_back(item.get());
    }
  }
  return found_items;
}

const std::vector<const character::Shopkeeper*> Model::ShopkeepersInRoom(const location::Room& room) const {
  std::vector<const character::Shopkeeper*> shopkeepers;
  for (int npc_id : room.NpcIds()) {
    const auto& character = Npc(npc_id);
    const character::Shopkeeper* shopkeeper = dynamic_cast<const character::Shopkeeper*>(&character);
    if (shopkeeper != nullptr) {
      shopkeepers.push_back(shopkeeper);
    }
  }
  return shopkeepers;
}

const std::vector<int> Model::ArmorIds(void) const {
  std::vector<int> armor_ids;
  for (const auto& item_pair : items_) {
    const auto& item = *item_pair.second;
    const item::Armor* armor = dynamic_cast<const item::Armor*>(&item);
    if (armor != nullptr) {
      armor_ids.push_back(armor->Id());
    }
  }
  return armor_ids;
}

const std::vector<int> Model::WeaponIds(void) const {
  std::vector<int> weapon_ids;
  for (const auto& item_pair : items_) {
    const auto& item = *item_pair.second;
    const item::Weapon* weapon = dynamic_cast<const item::Weapon*>(&item);
    if (weapon != nullptr) {
      weapon_ids.push_back(weapon->Id());
    }
  }
  return weapon_ids;
}

void Model::EquipItem(int player_id, const item::Item& item) {
  if (!players_.count(player_id)) {
    throw NoSuchElement("No such player with id " + player_id);
  }
  auto& player = *players_.at(player_id);

  const item::Armor* armor = dynamic_cast<const item::Armor*>(&item);
  if (armor != nullptr) {
    try {
      player.EquipArmor(armor->Id(), armor->GetWearLocation());
      return;
    }
    catch (...) {
      throw;
    }
  } 

  const item::Weapon* weapon = dynamic_cast<const item::Weapon*>(&item);
  if (weapon != nullptr) {
    try {
      player.EquipWeapon(weapon->Id());
      return;
    }
    catch (...) {
      throw;
    }
  }

  throw UnequippableItem("The item is unequippable");
}

void Model::Move(int player_id, int room_id) {
  if (!players_.count(player_id)) {
    throw NoSuchElement("No such player with id " + player_id);
  }
  if (!rooms_.count(room_id)) {
    throw NoSuchElement("No such room with id " + room_id);
  }
  auto& player = *players_.at(player_id);
  int moves = player.Moves();
  if (moves == 0) {
    throw PlayerExhausted("Player is exhausted");
  }
  auto& room = *rooms_.at(room_id);
  auto& room_old = *rooms_.at(player.LocationId());

  player.SetLocationId(room_id);
  player.SetMoves(--moves);
  room.AddPlayer(player_id);
  room_old.RemovePlayer(player_id);
}

}  // namespace model
