/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_MODEL_MODEL_H_
#define MUDGAME_SRC_MODEL_MODEL_H_

#include <algorithm>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "exception.h"
#include "character/character.h"
#include "character/player.h"
#include "character/shopkeeper.h"
#include "item/item.h"
#include "location/area.h"
#include "location/room.h"
#include "../controller/enum_information.h"

namespace model {

class Model {
 public:
  // Constructors
  Model(void) {}

  // Mutators
  void AddItem(std::unique_ptr<item::Item>&& item);
  void AddArea(std::unique_ptr<location::Area>&& area);
  void AddRoom(std::unique_ptr<location::Room>&& room);
  void AddConnection(std::unique_ptr<location::Connection>&& connection);
  void AddNpc(std::unique_ptr<character::Character>&& npc);
  void AddPlayer(std::unique_ptr<character::Player>&& player);

  // Accessors
  inline const std::map<int, std::unique_ptr<item::Item>>& Items(void) const {
    return items_;
  }
  inline const std::map<int, std::unique_ptr<location::Area>>& Areas(void)
        const {
    return areas_;
  }
  inline const std::map<int, std::unique_ptr<location::Room>>& Rooms(void)
        const {
    return rooms_;
  }
  inline const std::map<int, std::unique_ptr<location::Connection>>&
        Connections(void) const {
    return connections_;
  }
  inline const std::map<int, std::unique_ptr<character::Character>>& Npcs(void)
        const {
    return npcs_;
  }
  inline const std::map<int, std::unique_ptr<character::Player>>& Players(void)
        const {
    return players_;
  }
  inline const item::Item& Item(int item_id) const {
    if (!items_.count(item_id)) {
      throw NoSuchElement("Could not find item with id " + item_id);
    }
    return *items_.at(item_id);
  }
  inline const location::Area& Area(int area_id) const {
    if (!areas_.count(area_id)) {
      throw NoSuchElement("Could not find area with id " + area_id);
    }
    return *areas_.at(area_id);
  }
  inline const location::Room& Room(int room_id) const {
    if (!rooms_.count(room_id)) {
      throw NoSuchElement("Could not find room with id " + room_id);
    }
    return *rooms_.at(room_id);
  }
  inline const location::Connection& Connection(int connection_id) const {
    if (!connections_.count(connection_id)) {
      throw NoSuchElement("Could not find connection with id " + connection_id);
    }
    return *connections_.at(connection_id);
  }
  inline const character::Character& Npc(int npc_id) const {
    if (!npcs_.count(npc_id)) {
      throw NoSuchElement("Could not find npc with id " + npc_id);
    }
    return *npcs_.at(npc_id);
  }
  inline const character::Player& Player(int player_id) const {
    if (!players_.count(player_id)) {
      throw NoSuchElement("Could not find player with id " + player_id);
    }
    return *players_.at(player_id);
  }
  const std::vector<item::Item*> FindItemsByName(const std::string& item_name)
        const;
  const std::vector<const character::Shopkeeper*> ShopkeepersInRoom(
        const location::Room& room) const; 
  const std::vector<int> ArmorIds(void) const;
  const std::vector<int> WeaponIds(void) const;

  // Operations
  void GameTick(void);
  inline bool Login(int player_id, unsigned long long password_hash) {
    if (!players_.count(player_id)) {
      throw NoSuchElement("No such player with id " + player_id);
    }

    character::Player& player = *players_[player_id]; 
    try {
      return player.Login(password_hash);
    }
    catch (...) {
      throw;
    }
  }
  inline void Logout(int player_id) {
    if (!players_.count(player_id)) {
      throw NoSuchElement("No such player with id " + player_id);
    }

    character::Player& player = *players_[player_id]; 
    try {
      player.Logout();
    }
    catch (...) {
      throw;
    }
  }
  inline bool SetDoorStatus(int connection_id, bool open) {
    if (!connections_.count(connection_id)) {
      throw NoSuchElement("No such connection with id " + connection_id);
    }

    location::Connection& connection = *connections_[connection_id];
    return connection.SetOpen(open);
  }
  void EquipItem(int player_id, const item::Item& item);
  inline bool UnequipWeapon(int player_id) {
    if (!players_.count(player_id)) {
      throw NoSuchElement("No such player with id " + player_id);
    }

    character::Player& player = *players_[player_id];
    return player.UnequipWeapon();
  }
  inline bool UnequipArmor(int player_id, item::WearLocation wear_location) {
    if (!players_.count(player_id)) {
      throw NoSuchElement("No such player with id " + player_id);
    }

    character::Player& player = *players_[player_id];
    return player.UnequipArmor(wear_location);
  }
  void Move(int player_id, int room_id);
  inline int Buy(int player_id, const item::Item& item) {
    if (!players_.count(player_id)) {
      throw NoSuchElement("No such player with id " + player_id);
    }

    character::Player& player = *players_[player_id];
    try {
      return player.Buy(item);
    }
    catch (...) {
      throw;
    }
  }
  inline void AddToInventory(int player_id, const item::Item& item) {
    if (!players_.count(player_id)) {
      throw NoSuchElement("No such player with id " + player_id);
    }

    character::Player& player = *players_[player_id];
    player.AddToInventory(item.Id());
  }

 private:
  const int kDefaultPlayerSpawn = 4;
  std::map<int, std::unique_ptr<item::Item>> items_;
  std::map<int, std::unique_ptr<location::Area>> areas_;
  std::map<int, std::unique_ptr<location::Room>> rooms_;
  std::map<int, std::unique_ptr<location::Connection>> connections_;
  std::map<int, std::unique_ptr<character::Character>> npcs_;
  std::map<int, std::unique_ptr<character::Player>> players_;
};

}  // namespace model

#endif  //  MUDGAME_SRC_MODEL_MODEL_H_
