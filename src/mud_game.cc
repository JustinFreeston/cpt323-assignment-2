/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "mud_game.h"

int main(int argc, char** argv) {
  std::unique_ptr<CommandLineArgs> cla = nullptr;

  try {
    cla = std::make_unique<CommandLineArgs>(argc, argv);
    CommandLineArgs cla(argc, argv);
    if (cla.IsHelp()) {
      std::cout << cla.Help() << std::endl;
      return EXIT_SUCCESSFUL;
    }
  }
  catch (const boost::program_options::invalid_command_line_syntax& e) {
    std::cout << "Invalid_command_line_syntax" << std::endl;
  }
  catch (const boost::program_options::unknown_option& e) {
    std::cout << "Unknown_option" << std::endl;
  }

//   std::cout << "AsciiLoad: \"" << cla->AsciiLoad() << "\"" << std::endl;
//   std::cout << "BinaryLoad: \"" << cla->BinaryLoad() << "\"" << std::endl;
//   std::cout << "AsciiSave: \"" << cla->AsciiSave() << "\"" << std::endl;
//   std::cout << "BinarySave: \"" << cla->BinarySave() << "\"" << std::endl;

  try {
    game::Game game(*cla);
    game.Load();
    game.Start();
  }
  catch (const std::exception& e) {
    std::cout << "Error: " << e.what() << std::endl;
  }

  std::cout << "Thanks for playing!" << std::endl;
}

