/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "utility.h"

namespace utility {

std::vector<std::string> Utility::Split(const std::string& text, char delim) {
  boost::char_separator<char> sep(&delim);
  std::vector<std::string> elements;

  boost::tokenizer<boost::char_separator<char>> tok{text, sep};
  for (auto iter = tok.begin(); iter != tok.end(); ++iter) {
    elements.push_back(*iter);
  }

  return elements;
}

std::string Utility::Join(const std::vector<std::string>& strings,
                        const std::string& delim, int start, int end) {
  std::string joined;
  for (int i = start; i < end; ++i) {
    joined += strings[i];
    joined += delim;
  }
  if (joined.length() <= 1) {
    return "";
  }
  return joined.substr(0, joined.length() - 1);
}

std::string Utility::Lower(const std::string& text) {
  std::string temp = text;
  std::transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
  return temp;
}

std::string Utility::Upper(const std::string& text) {
  std::string temp = text;
  std::transform(temp.begin(), temp.end(), temp.begin(), ::toupper);
  return temp;
}

bool Utility::StartsWith(const std::string& text, const std::string& find) {
  return text.compare(0, find.size(), find) == 0;
}

// https://stackoverflow.com/questions/16177295/get-time-since-epoch-in-milliseconds-preferably-using-c11-chrono
unsigned long Utility::CurrentTimeMs(void) {
  return std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1);
}

}  // namespace utility
