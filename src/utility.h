/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MUDGAME_SRC_UTILITY_H_
#define MUDGAME_SRC_UTILITY_H_

#include <algorithm>
#include <chrono>
#include <string>
#include <vector>

#include <boost/tokenizer.hpp>

namespace utility {

struct Utility {
  static std::vector<std::string> Split(const std::string& text, char delim);
  static std::string Join(const std::vector<std::string>& strings, const std::string& delim, int start, int end);
  static std::string Lower(const std::string& text);
  static std::string Upper(const std::string& text);
  static bool StartsWith(const std::string& text, const std::string& find);
  static unsigned long CurrentTimeMs(void);
};

}  // namespace utility

#endif  // MUDGAME_SRC_UTILITY_H_
