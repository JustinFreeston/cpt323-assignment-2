/* MIT License
* 
* Copyright (c) 2017 Justin Freeston <s3547990@student.rmit.edu.au>
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include "catch.hpp"

#include "../src/model/location/room.h"

namespace ml = model::location;

int CountNotNull(std::array<int, ml::kTotalDirections> ids) {
  int count = 0;
  for (int i = 0; i < ids.size(); ++i) {
    if (ids[i]) {
      ++count;
    }
  }
  return count;
}

//  GlobalId()
//  LocalId()
//  AreaId()
//  Name()
//  Description()
//  AddConnection(1)
//  RemoveConnection(1)
//  Connections() // array<connection_id, Direction.kTotalDirections>
//  AddCharacter(1)
//  RemoveCharacter(1)
//  Characters() // vector<character_id>
TEST_CASE("Room test", "[room]") {
  // Test basic info
  ml::Room room(1, 2, 3, "name", "description");
  REQUIRE(room.GlobalId() == 1);
  REQUIRE(room.LocalId() == 2);
  REQUIRE(room.AreaId() == 3);
  REQUIRE(room.Name() == "name");
  REQUIRE(room.Description() == "description");

  // Test connections
  room.AddConnection(1, ml::Direction::kNorth);
  room.AddConnection(2, ml::Direction::kUp);
  room.AddConnection(3, ml::Direction::kEast);

  REQUIRE(CountNotNull(room.ConnectionIds()) == 3);

  room.AddConnection(4, ml::Direction::kNorth);
  REQUIRE(CountNotNull(room.ConnectionIds()) == 3);

  room.RemoveConnection(ml::Direction::kEast);
  REQUIRE(CountNotNull(room.ConnectionIds()) == 2);

  REQUIRE_THROWS_AS(room.RemoveConnection(ml::Direction::kDown), model::NoSuchElement);

  REQUIRE(room.ConnectionId(ml::Direction::kNorth) == 4);

  // Test characters
  room.AddCharacter(1);
  room.AddCharacter(2);
  REQUIRE(room.CharacterIds().size() == 2);

  REQUIRE_THROWS_AS(room.RemoveCharacter(3), model::NoSuchElement);

  room.RemoveCharacter(1);
  REQUIRE(room.CharacterIds().size() == 1);
  REQUIRE(room.CharacterIds().front() == 2);
}

